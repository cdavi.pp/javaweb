<?php 
    session_start();
    if(!isset($_SESSION['id'])){
        header("Location: index.php");
    }
?>
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#"><span class="text-muted">X</span>Finanças</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
    <ul class="navbar-nav mr-auto">
        <li class="nav-item">
            <a class="nav-link" href="frontSis.php">Home<span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="entrada.php">Registros de Entrada<span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="saida.php">Registro de Saída<span class="sr-only">(current)</span></a>
        </li>
    </ul>
    <ul class="nav justify-content-end">
        <li class="nav-item">
            <a href="logout.php" class="btn btn-outline-danger">logout</a>
        </li>
    </ul>
    </div>
</nav><br><br>