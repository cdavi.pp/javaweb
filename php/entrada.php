<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Registros de Entrada</title>
        <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.min.css">
    </head>
    <body>
        <header>
            <?php require_once("navBar.php"); ?>
            <section class="jumbotron text-center">
                <div class="container">
                    <h1 class="jumbotron-heading">Registros de Entrada</h1>
                    <p class="lead text-muted">Todas as entradas são registradas e essas informações são mostradas nesta página</a></p>
                    <div id="funcionalidades"></div>
                </div>
            </section>
        </header>
        <main role="main">
            <div class="content container marketing">
                <center><h2>Entradas</h2></center>
                <br>
                <hr>
                <center>
                    <button type='button' class='btn btn-primary' data-toggle='modal' data-target='#modalRegistroEntrada'>Criar um novo registro</button>
                </center>
                <hr>
                <table class="table table-striped table-dark">
                    <thead>
                        <tr>
                            <th scope="col">Data</th>
                            <th scope="col">Valor</th>
                            <th scope="col">Tipo</th>
                            <th scope="col">Local</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                    require_once("src/Controle/ControleRegistroEntrada.class.php");
                    require_once("src/Controle/ControleCorrente.class.php");
                    require_once("src/Controle/ControlePoupanca.class.php");
                    $controleEntrada = new ControleRegistroEntrada();
                    $controleCorrente = new ControleCorrente();
                    $controlePoupanca = new ControlePoupanca();
                    $registrosEntrada = $controleEntrada->mostrarTodosRegistros($_SESSION['id']);
                    foreach($registrosEntrada as $item){
                        $data = new DateTime($item->getData());
                        echo "
                        <tr>
                            <td>" . $data->format('d/m/Y') . "</td>
                            <td>" . $item->getValor() . "</td>
                            <td>" . $item->getTipo() . "</td>
                            <td>";
                            if($item->getCarteiraId() != NULL){
                                echo "Carteira";
                            }else if($item->getCorrenteId()!=NULL){
                                $contaCorrente = $controleCorrente->mostrarCorrente($item->getCorrenteId());
                                echo "Corrente - " . $contaCorrente->getBanco();
                            }else if($item->getPoupancaId()!=NULL){
                                $contaPoupanca = $controlePoupanca->mostrarPoupanca($item->getPoupancaId());
                                echo "Poupança - " . $contaPoupanca->getBanco();
                            }
                            echo "</td>
                        </tr>";
                    }
                    ?>
                    </tbody>
                </table>
                <br>
            </div>
            <?php 
            require_once("footer.php");
            require_once("modalEntrada.php");
            ?>
        </main>
    </body>
    <script src='js/jquery.js'></script>
    <script src="bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>
    <script src="bootstrap-4.3.1-dist/js/bootstrap.bundle.js"></script>
    <script src="js/ajax.js"></script>
    <script src="js/entradaAjax.js"></script>
    <script>
        $("#registroEntrada").submit(function(){
            var dadosEntrada = $(this).serialize();
            $.post("criarRegistroEntrada.php", dadosEntrada, function(resultado){
                if(resultado == 0){
                    $("#erroGeral").modal("show");
                }else if(resultado == 1){
                    $("#modalRegistroEntrada").modal("hide");
                    $("#sucessoRegistroEntrada").modal("show");
                }
            });
            return false;
        });
        $("#sucessoRegistroEntrada").on("hide.bs.modal", function(){
            window.location.replace("index.php");
        });
    </script>
</html>