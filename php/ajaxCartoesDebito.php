<div class="form-group">
	<label for="recipient-cartaoId">Cartão: </label>
	<select class="form-control" name='cartaoId' id="recipient-cartaoId" required>
		<option>Escolha um cartão</option>
		<?php 
			require_once("src/Controle/ControleCartao.class.php");
			require_once("src/Controle/ControleCorrente.class.php");
			session_start();
			$controleCartao = new ControleCartao();
			$cartoes = $controleCartao->mostrarCartoesDebito($_SESSION['id']);
			$controleCorrente = new ControleCorrente();
			foreach($cartoes as $item){
				echo "<option value=" . $item->getId() . ">";
				$corrente = $controleCorrente->mostrarCorrente($item->getCorrenteId());
				echo $corrente->getBanco();
				echo "</option>";
			}
		?>
	</select>
</div>