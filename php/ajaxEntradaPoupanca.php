<div class="form-group">
	<label for="recipient-poupancaId">Conta Poupança: </label>
	<select class="form-control" name='poupancaId' id="recipient-poupancaId" required>
		<option>Escolha uma opção</option>
		<?php 
			require_once("src/Controle/ControlePoupanca.class.php");
			session_start();
			$controlePoupanca = new ControlePoupanca();
			$contasPoupanca = $controlePoupanca->mostrarTodasPoupancas($_SESSION['id']);
			foreach($contasPoupanca as $item){
				echo "<option value=" . $item->getId() . ">" . $item->getBanco() . "</option>";
			}
		?>
	</select>
</div>