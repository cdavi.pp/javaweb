<?php
require_once("src/Modelo/ModeloRegistroEntrada.class.php");
require_once("src/Controle/ControleRegistroEntrada.class.php");
session_start();
$regEnt = new RegistroEntrada();
$controleRegistro = new ControleRegistroEntrada();
$regEnt->setValor($_POST['valor']);
$data = new DateTime($_POST['data']);
$regEnt->setData($data->format("Y-m-d"));
$regEnt->setTipo($_POST['tipo']);
$regEnt->setUsuarioId($_SESSION['id']);
if($_POST['forma'] == "carteira"){
	$regEnt->setCarteiraId($_POST['carteiraId']);
	$controleRegistro->criarRegistro($regEnt);
	require_once("adicionarFundosCarteira.php");
}else if($_POST['forma'] == "corrente"){
	$regEnt->setCorrenteId($_POST['correnteId']);
	$controleRegistro->criarRegistro($regEnt);
	require_once("adicionarFundosCorrente.php");
}else if($_POST['forma'] == "poupanca"){
	$regEnt->setPoupancaId($_POST['poupancaId']);
	$controleRegistro->criarRegistro($regEnt);
	require_once("adicionarFundosPoupanca.php");
}

?>