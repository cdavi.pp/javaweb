
<!--Modal para mostrar formulário de criação de um registro de saída -->
<div class="modal fade" id="modalRegistroSaida" tabindex="-1" role="dialog" aria-labelledby="exampleModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title w-100 font-weight-bold" id="exampleModalTitle">Novo Registro de Saida</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="criarRegistroSaida.php" method='POST' id='registroSaida'>
                <div class="modal-body mx-3">
                    <div class="form-group">
                        <label for="recipient-dataRegistro" class="col-from-label">Data do Registro/Compra: </label>
                        <input type="date" name='dataRegistro' class="form-control" id="recipient-dataRegistro" required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-tipo">Tipo da Saida: </label>
                        <select class="form-control" name='tipo' id="recipient-tipo" required>
                            <option>Escolha uma opção</option>
                            <option value="Herança">Compra de Mercadoria</option>
                            <option value="Investimentos Financeiros">Compra de Móveis</option>
                            <option value="Doação">Cinema</option>
                            <option value="Empréstimo">Restaurante</option>
                            <option value="outroTipo">Outro...</option>
                            </select>
                    </div>
                    <div id="ajaxOutroTipo"></div>
                    <div class="form-group">
                        <label for="recipient-forma">Forma de Pagamento: </label>
                        <select class="form-control" name='forma' id="recipient-forma" required>
                            <option>Escolha uma opção</option>
                            <option value="carteira">Carteira</option>
                            <option value="debito">Cartão de Débito</option>
                            <!-- Opção desabilitada <option value="credito">Cartão de Crédito</option>-->
                        </select>
                    </div>
                    <div id='ajaxSaida'></div>
                    <div class="form-group">
                        <label for="recipient-valor" class="col-from-label">Valor: </label>
                        <input type="number" name='valor' step='.01' class="form-control" id="recipient-valor" placeholder="Ex.: 1000,99" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" data-dismiss="modal" type="button">Cancelar</button>
                    <input type='submit' class="btn btn-primary" type="button" value='Criar'>
                </div>
            </form>
        </div>
    </div>
</div>

<!--Modal para avisar que houve um erro na aplicação -->
<div class="modal fade" id="erroGeral" tabindex="-1" role="dialog" aria-labelledby="exampleModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title w-100 font-weight-bold" id="exampleModalTitle">Algo deu errado :(</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <img src="img/error.png" width="30%" height="100%" alt="">
                Ocorreu um erro, atualize a página e tente novamente.
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" data-dismiss="modal" type="button">Ok</button>
            </div>
        </div>
    </div>
</div>

<!--Modal para avisar que houve êxito no processo de criar registro -->
<div class="modal fade" id="sucessoRegistroSaida" tabindex="-1" role="dialog" aria-labelledby="exampleModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title w-100 font-weight-bold" id="exampleModalTitle">Sucesso</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <img src="img/success.png" width="30%" height="100%" alt="">
                Seu registro foi criado.
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" data-dismiss="modal" type="button">Ok</button>
            </div>
        </div>
    </div>
</div>