<?php
require_once("src/Controle/ControleRegistroSaida.class.php");
require_once("src/Modelo/ModeloRegistroSaida.class.php");
require_once("src/Controle/ControleCartao.class.php");
require_once("src/Controle/ControleCorrente.class.php");
session_start();
$regSaida = new RegistroSaida();
$controleSaida = new ControleRegistroSaida();
$controleCartao = new ControleCartao();
$valor = $_POST['valor'];
$dataReg = $_POST['dataRegistro'];
$regSaida->setUsuarioId($_SESSION['id']);
if($_POST['forma'] == 'carteira'){
	$regSaida->setValor($valor);
	$regSaida->setDataRegistro($dataReg);
	$regSaida->setDataPagamento($dataReg);
	$regSaida->setTipo($_POST['tipo']);
	$regSaida->setStatus(1);
	$regSaida->setCarteiraId($_POST['carteiraId']);
	$controleSaida->criarRegistro($regSaida);
	require_once("retirarFundosCarteira.php");
}else if($_POST['forma'] == 'debito'){
	$regSaida->setValor($valor);
	$regSaida->setDataRegistro($dataReg);
	$regSaida->setDataPagamento($dataReg);
	$regSaida->setTipo($_POST['tipo']);
	$regSaida->setStatus(1);
	$regSaida->setCartaoId($_POST['cartaoId']);
	$controleCorrente = new ControleCorrente();
	$cartao = $controleCartao->mostrarCartao($_POST['cartaoId']);
	$correnteId = $cartao->getCorrenteId();
	$controleSaida->criarRegistro($regSaida);
	require_once("retirarFundosCorrente.php");
}else if($_POST['forma'] == 'credito'){
	$regSaida->setValor($valor);
	$regSaida->setDataRegistro($dataReg);
	$regSaida->setTipo($_POST['tipo']);
	$regSaida->setCartaoId($_POST['cartaoId']);	
}
?>