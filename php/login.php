<?php
    require_once("src/Modelo/ModeloUsuario.class.php");
    require_once("src/Controle/ControleUsuario.class.php");
    session_start();
    $usuario = new Usuario();
    $controle = new ControleUsuario();
    $usuario->setEmail($_POST['email']);
    $senha = $_POST['senha'];
    $salt = "bJdpGaBSYRrgRStF";
    for($a=0;$a<640;$a++){
        $salt = md5($salt);
    }
    $crypt = crypt($senha, $salt);
    $usuario->setSenha($crypt);
    $resu = $controle->logarUsuario($usuario);
    if($resu != false){
        $_SESSION['id'] = $resu;
        echo 1;
    }else{
        echo 0;
    }
?>