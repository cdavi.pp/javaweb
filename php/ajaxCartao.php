<div class="form-group">
	<label for="recipient-cartaoId">Cartão: </label>
	<select class="form-control" name='cartaoId' id="recipient-cartaoId" required>
		<option>Escolha uma opção</option>
		<?php 
			require_once("src/Controle/ControleCartao.class.php");
			require_once("src/Controle/ControleCorrente.class.php");
			session_start();
			$controleCartao = new ControleCartao();
			$cartoes = $controleCartao->mostrarTodosCartoes($_SESSION['id']);
			$controleCorrente = new ControleCorrente();
			foreach($cartoes as $item){
				echo "<option value=" . $item->getId() . ">";
				if($item->getTipo()=="credito"){
					echo "Crédito - " . $item->getNumero();
				}else if($item->getTipo()=="debito"){
					echo "Débito - ";
					$corrente = $controleCorrente->mostrarCorrente($item->getCorrenteId());
					echo $corrente->getBanco();
				}else if($item->getTipo()=="credeb"){
					echo "Crédito/Débito - ";
					$corrente = $controleCorrente->mostrarCorrente($item->getCorrenteId());
					echo $corrente->getBanco();
				}
				echo "</option>";
			}
		?>
	</select>
</div>