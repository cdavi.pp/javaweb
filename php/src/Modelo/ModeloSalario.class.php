<?php
    class Salario{
        private $id;
        private $valor;
        private $data;
        private $usuario_id;

        public function setId($i){
            $this->id=($i!=NULL) ? addslashes ($i) : NULL;
        }

        public function getId(){
            return $this->id;
        }

        public function setValor($v){
            $this->valor= ($v!=NULL) ? addslashes ($v) : NULL;
        }

        public function getValor(){
            return $this->valor;
        }

        public function setData($d){
            $this->data=($d!=NULL) ? addslashes ($d) : NULL;
        }

        public function getData(){
            return $this->data;
        }

         public function setUsuarioId($ui){
            $this->usuario_id=($ui!=NULL) ? addslashes ($ui) :  NULL;
        }

        public function getUsuarioId(){
            return $this->usuario_id;
        }
        
    }
?>