<?php
    class Usuario{
        private $id;
        private $nome;
        private $email;
        private $senha;

        public function setId($i){
            $this->id=($i!=NULL) ? addslashes ($i) : NULL;
        }

        public function getId(){
            return $this->id;
        }

        public function setNome($n){
            $this->nome = ($n != NULL) ? addslashes($n) : NULL;
        }
        public function getNome(){
            return $this->nome;
        }

        public function setEmail($e){
            $this->email = ($e != NULL) ? addslashes($e) : NULL;
        }
        public function getEmail(){
            return $this->email;
        }

        public function setSenha($s){
            $this->senha = ($s != NULL) ? addslashes($s) : NULL;
        }
        public function getSenha(){
            return $this->senha;
        }
    
    } 
?>