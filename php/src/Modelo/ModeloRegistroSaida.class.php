<?php
    class RegistroSaida{
        private $id;
        private $valor;
        private $dataRegistro;
        private $dataPagamento;
        private $tipo;
        private $status;
        private $usuario_id;
        private $cartao_id;
        private $corrente_id;
        private $carteira_id;

        public function setId($i){
            $this->id=($i!=NULL) ? addslashes ($i) : NULL;
        }

        public function getId(){
            return $this->id;
        }

        public function setValor($v){
            $this->valor= ($v!=NULL) ? addslashes ($v) : NULL;
        }

        public function getValor(){
            return $this->valor;
        }

        public function setDataRegistro($dr){
            $this->dataRegistro=($dr!=NULL) ? addslashes ($dr) : NULL;
        }

        public function getDataRegistro(){
            return $this->dataRegistro;
        }

        public function setDataPagamento($dp){
            $this->dataPagamento=($dp!=NULL) ? addslashes ($dp) : NULL;
        }

        public function getDataPagamento(){
            return $this->dataPagamento;
        }

        public function setTipo($t){
            $this->tipo=($t!=NULL) ? addslashes ($t) : NULL;
        }

        public function getTipo(){
            return $this->tipo;
        }

        public function setStatus($s){
            $this->status=($s!=NULL) ? addslashes ($s) : NULL;
        }

        public function getStatus(){
            return $this->status;
        }

        public function setUsuarioId($ui){
            $this->usuario_id=($ui!=NULL) ? addslashes ($ui) :  NULL;
        }

        public function getUsuarioId(){
            return $this->usuario_id;
        }

        public function setCartaoId($ci){
            $this->cartao_id=($ci!=NULL) ? addslashes ($ci) :  NULL;
        }

        public function getCartaoId(){
            return $this->cartao_id;
        }

        public function setCorrenteId($ci){
            $this->corrente_id=($ci!=NULL) ? addslashes ($ci) :  NULL;
        }

        public function getCorrenteId(){
            return $this->corrente_id;
        }

        public function setCarteiraId($ci){
            $this->carteira_id=($ci!=NULL) ? addslashes ($ci) :  NULL;
        }

        public function getCarteiraId(){
            return $this->carteira_id;
        }

    }
?>