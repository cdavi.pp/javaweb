<?php
    class Poupanca{
        private $id;
        private $saldo;
        private $data;
        private $banco;
        private $usuario_id;

        public function setId($i){
            $this->id=($i!=NULL) ? addslashes ($i) : NULL;
        }

        public function getId(){
            return $this->id;
        }

        public function setSaldo($s){
            $this->saldo= ($s!=NULL) ? addslashes ($s) : NULL;
        }

        public function getSaldo(){
            return $this->saldo;
        }

        public function setData($d){
            $this->data=($d!=NULL) ? addslashes ($d) : NULL;
        }

        public function getData(){
            return $this->data;
        }

        public function setBanco($b){
            $this->banco=($b!=NULL) ? addslashes ($b) : NULL;
        }

        public function getBanco(){
            return $this->banco;
        }

        public function setUsuarioId($ui){
            $this->usuario_id=($ui!=NULL) ? addslashes ($ui) :  NULL;
        }

        public function getUsuarioId(){
            return $this->usuario_id;
        }
    }
?>