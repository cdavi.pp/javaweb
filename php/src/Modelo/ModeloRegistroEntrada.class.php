<?php
    class RegistroEntrada{
        private $id;
        private $valor;
        private $data;
        private $tipo;
        private $usuario_id;
        private $poupanca_id;
        private $corrente_id;
        private $carteira_id;

        public function setId($i){
            $this->id=($i!=NULL) ? addslashes ($i) : NULL;
        }

        public function getId(){
            return $this->id;
        }

        public function setValor($v){
            $this->valor= ($v!=NULL) ? addslashes ($v) : NULL;
        }

        public function getValor(){
            return $this->valor;
        }

        public function setData($d){
            $this->data=($d!=NULL) ? addslashes ($d) : NULL;
        }

        public function getData(){
            return $this->data;
        }

        public function setTipo($t){
            $this->tipo=($t!=NULL) ? addslashes ($t) : NULL;
        }

        public function getTipo(){
            return $this->tipo;
        }

       public function setUsuarioId($ui){
            $this->usuario_id=($ui!=NULL) ? addslashes ($ui) :  NULL;
        }

        public function getUsuarioId(){
            return $this->usuario_id;
        }

        public function setPoupancaId($pi){
            $this->poupanca_id=($pi!=NULL) ? addslashes ($pi) :  NULL;
        }

        public function getPoupancaId(){
            return $this->poupanca_id;
        }

        public function setCorrenteId($ci){
            $this->corrente_id=($ci!=NULL) ? addslashes ($ci) :  NULL;
        }

        public function getCorrenteId(){
            return $this->corrente_id;
        }

        public function setCarteiraId($ci){
            $this->carteira_id=($ci!=NULL) ? addslashes ($ci) :  NULL;
        }

        public function getCarteiraId(){
            return $this->carteira_id;
        }

    }
?>