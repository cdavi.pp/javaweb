<?php
    class Carteira{
        private $id;
        private $saldo;
        private $usuario_id;

        public function setId($i){
            $this->id=($i!=NULL) ? addslashes ($i) : NULL;
        }

        public function getId(){
            return $this->id;
        }

        public function setSaldo($s){
            $this->saldo=($s!=NULL) ? addslashes ($s) : NULL;
        }

        public function getSaldo(){
            return $this->saldo;
        }

       public function setUsuarioId($ui){
            $this->usuario_id=($ui!=NULL) ? addslashes ($ui) :  NULL;
        }

        public function getUsuarioId(){
            return $this->usuario_id;
        }

    }
?>