<?php
    class Cartao{
        private $id;
        private $numero;
        private $limiteVista;
        private $limitePrazo;
        private $melhorDia;
        private $tipo;
        private $dataVencimento;
        private $usuario_id;
        private $corrente_id;

        public function setId($i){
            $this->id=($i!=NULL) ? addslashes ($i) : NULL;
        }

        public function getId(){
            return $this->id;
        }

        public function setNumero($n){
            $this->numero=($n!=NULL) ? addslashes ($n) : NULL;
        }

        public function getNumero(){
            return $this->numero;
        }

        public function setLimiteVista($lv){
            $this->limiteVista=($lv!=NULL) ? addslashes ($lv) : NULL;
        }

        public function getLimiteVista(){
            return $this->limiteVista;
        }

        public function setLimitePrazo($lp){
            $this->limitePrazo=($lp!=NULL) ? addslashes ($lp) : NULL;
        }

        public function getLimitePrazo(){
            return $this->limitePrazo;
        }        

        public function setMelhorDia($md){
            $this->melhorDia=($md!=NULL) ? addslashes ($md) :  NULL;
        }

        public function getMelhorDia(){
            return $this->melhorDia;
        }

        public function setTipo($t){
            $this->tipo=($t!=NULL) ? addslashes ($t) :  NULL;
        }

        public function getTipo(){
            return $this->tipo;
        }
        
        public function setDataVencimento($dv){
            $this->dataVencimento=($dv!=NULL) ? addslashes ($dv) : NULL;
        }

        public function getDataVencimento(){
            return $this->dataVencimento;
        }

        public function setUsuarioId($ui){
            $this->usuario_id=($ui!=NULL) ? addslashes ($ui) :  NULL;
        }

        public function getUsuarioId(){
            return $this->usuario_id;
        }

        public function setCorrenteId($ci){
            $this->corrente_id=($ci!=NULL) ? addslashes ($ci) :  NULL;
        }

        public function getCorrenteId(){
            return $this->corrente_id;
        }

    }

?>