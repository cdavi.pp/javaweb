<?php
    require_once("lib/Conection.class.php");
    require_once("src/Modelo/ModeloPoupanca.class.php");
    class ControlePoupanca{
        public function CriarPoupanca($poupanca){
            $conexao = new Conection("lib/xuxu.ini");
            $sql = "INSERT INTO poupanca(saldo, data, banco, usuario_id) VALUE(:sa, :da, :ba, :idu)";
            $comando = $conexao->getConection()->prepare($sql);
            $comando->bindValue(":sa",$poupanca->getSaldo());
            $comando->bindValue(":da",$poupanca->getData());
            $comando->bindValue(":ba",$poupanca->getBanco());
            $comando->bindValue(":idu",$poupanca->getUsuarioId());
            if($comando->execute()){
                $conexao->__destruct();
                return true;
            }else{
                $conexao->__destruct();
                return false;
            }
        }
        public function MostrarTodasPoupancas($id){
            $conexao = new Conection("lib/xuxu.ini");
            $sql = "SELECT * FROM poupanca WHERE usuario_id = :id";
            $comando = $conexao->getConection()->prepare($sql);
            $comando->bindParam(":id", $id);
            $comando->execute();
            $resu = $comando->fetchAll();
            $lista = array();
            foreach($resu as $item){
                $poupanca = new Poupanca();
                $poupanca->setId($item->id);
                $poupanca->setSaldo($item->saldo);
                $poupanca->setData($item->data);
                $poupanca->setBanco($item->banco);
                $poupanca->setUsuarioId($item->usuario_id);
                array_push($lista, $poupanca);
            }
            $conexao->__destruct();
            return $lista;
        }

        public function MostrarPoupanca($id){
            $conexao = new Conection("lib/xuxu.ini");
            $sql = "SELECT * FROM poupanca WHERE id = :id";
            $comando = $conexao->getConection()->prepare($sql);
            $comando->bindParam(":id", $id);
            $comando->execute();
            $resu = $comando->fetch();
            $conexao->__destruct();
            $poupanca = new Poupanca();
            $poupanca->setId($resu->id);
            $poupanca->setSaldo($resu->saldo);
            $poupanca->setData($resu->data);
            $poupanca->setBanco($resu->banco);
            $poupanca->setUsuarioId($resu->usuario_id);
            return $poupanca;
        }

        public function AdicionarFundos($id, $valor){
            $conexao = new Conection("lib/xuxu.ini");
            $sql = "UPDATE poupanca SET saldo = saldo + :valor WHERE id=:id;";
            $comando = $conexao->getConection()->prepare($sql);
            $comando->bindParam(":id", $id);
            $comando->bindParam(":valor",$valor);
            if($comando->execute()){
                $conexao->__destruct();
                return true;
            }else{
                $conexao->__destruct();
                return false;
            }
            $conexao->__destruct();
        }
        public function RetirarFundos($id, $valor){
            $conexao = new Conection("lib/xuxu.ini");
            $sql = "UPDATE poupanca SET saldo = saldo - :valor WHERE id=:id;";
            $comando = $conexao->getConection()->prepare($sql);
            $comando->bindParam(":id", $id);
            $comando->bindParam(":valor",$valor);
            if($comando->execute()){
                $conexao->__destruct();
                return true;
            }else{
                $conexao->__destruct();
                return false;
            }
        }
        public function AtualizacaoMensal($id){
            $conexao = new Conection("lib/xuxu.ini");
            $sql = "UPDATE  poupanca SET saldo = (saldo * 0.005) + saldo, data = ADDDATE(data, INTERVAL 1 MONTH) WHERE id=:id;";
            $comando = $conexao->getConection()->prepare($sql);
            $comando->bindParam(":id",$id);
            if($comando->execute()){
                $conexao->__destruct();
                return true;
            }else{
                $conexao->__destruct();
                return false;
            }
        }
    }
?>