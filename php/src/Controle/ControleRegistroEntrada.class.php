<?php
require_once("src/Modelo/ModeloRegistroEntrada.class.php");
require_once("lib/Conection.class.php");
class ControleRegistroEntrada{
	//Método para fazer a criação do registro
	public function criarRegistro($regEnt){
		$connection = new Conection("lib/xuxu.ini");
		$sql = "INSERT INTO registroEntrada(valor, data, tipo, usuario_id, corrente_id, poupanca_id, carteira_id) VALUES(:v, :d, :t, :ui, :co, :p, :ca)";
		$comando = $connection->getConection()->prepare($sql);
		$comando->bindValue(":v", $regEnt->getValor());
		$comando->bindValue(":d", $regEnt->getData());
		$comando->bindValue(":t", $regEnt->getTipo());
		$comando->bindValue(":ui", $regEnt->getUsuarioId());
		$comando->bindValue(":co", $regEnt->getCorrenteId());
		$comando->bindValue(":p", $regEnt->getPoupancaId());
		$comando->bindValue(":ca", $regEnt->getCarteiraId());
		if($comando->execute()){
			$connection->__destruct();
			return true;
		}else{
			$connection->__destruct();
			return false;
		}
	}
	//Método para mostrar todos os registros de um usuário (inner join para especificar qual a poupança ou qual corrente está sendo usada, caso seus campos estejam preenchidos)
	public function mostrarTodosRegistros($usuarioId){
		$connection = new Conection("lib/xuxu.ini");
		$sql = "SELECT * FROM registroEntrada WHERE usuario_id = :id ORDER BY data DESC";
		$comando = $connection->getConection()->prepare($sql);
		$comando->bindParam(":id", $usuarioId);
		$comando->execute();
		$resul = $comando->fetchAll();
		$connection->__destruct();
		$listaRegEnt = array();
		foreach($resul as $item){
			$regEnt = new RegistroEntrada();
			$regEnt->setId($item->id);
			$regEnt->setValor($item->valor);
			$regEnt->setData($item->data);
			$regEnt->setTipo($item->tipo);
			$regEnt->setUsuarioId($item->usuario_id);
			$regEnt->setCorrenteId($item->corrente_id);
			$regEnt->setPoupancaId($item->poupanca_id);
			$regEnt->setCarteiraId($item->carteira_id);
			array_push($listaRegEnt, $regEnt);
		}
		return $listaRegEnt;
	}
	
	public function mostrarTodosRegistrosCorrente($usuarioId){
		$connection = new Conection("lib/xuxu.ini");
		$sql = "SELECT * FROM registroEntrada WHERE usuario_id = :id AND corrente_id <> 'NULL' ORDER BY data DESC";
		$comando = $connection->getConection()->prepare($sql);
		$comando->bindParam(":id", $usuarioId);
		$comando->execute();
		$resul = $comando->fetchAll();
		$connection->__destruct();
		$listaRegEnt = array();
		foreach($resul as $item){
			$regEnt = new RegistroEntrada();
			$regEnt->setId($item->id);
			$regEnt->setValor($item->valor);
			$regEnt->setData($item->data);
			$regEnt->setTipo($item->tipo);
			$regEnt->setUsuarioId($item->usuario_id);
			$regEnt->setCorrenteId($item->corrente_id);
			$regEnt->setPoupancaId($item->poupanca_id);
			$regEnt->setCarteiraId($item->carteira_id);
			array_push($listaRegEnt, $regEnt);
		}
		return $listaRegEnt;
	}
	public function mostrarTodosRegistrosPoupanca($usuarioId){
		$connection = new Conection("lib/xuxu.ini");
		$sql = "SELECT re.*, p.banco AS bancoPoupanca FROM registroEntrada AS re LEFT JOIN poupanca AS p ON re.corrente_id = p.id WHERE re.usuario_id = :id AND re.poupanca_id <> 'NULL'  ORDER BY data DESC";
		$comando = $connection->getConection()->prepare($sql);
		$comando->bindParam(":id", $usuarioId);
		$comando->execute();
		$resul = $comando->fetchAll();
		$connection->__destruct();
		$listaRegEnt = array();
		foreach($resul as $item){
			$regEnt = new RegistroEntrada();
			$regEnt->setId($item->id);
			$regEnt->setValor($item->valor);
			$regEnt->setData($item->data);
			$regEnt->setTipo($item->tipo);
			$regEnt->setUsuarioId($item->usuario_id);
			$regEnt->setCorrenteId($item->corrente_id);
			$regEnt->setPoupancaId($item->poupanca_id);
			$regEnt->setCarteiraId($item->carteira_id);
			array_push($listaRegEnt, $regEnt);
		}
		return $listaRegEnt;
	}

	public function mostrarTodosRegistrosCarteira($usuarioId){
		$connection = new Conection("lib/xuxu.ini");
		$sql = "SELECT * FROM registroEntrada WHERE usuario_id = :id AND carteira_id <> 'NULL'  ORDER BY data DESC";
		$comando = $connection->getConection()->prepare($sql);
		$comando->bindParam(":id", $usuarioId);
		$comando->execute();
		$resul = $comando->fetchAll();
		$connection->__destruct();
		$listaRegEnt = array();
		foreach($resul as $item){
			$regEnt = new RegistroEntrada();
			$regEnt->setId($item->id);
			$regEnt->setValor($item->valor);
			$regEnt->setData($item->data);
			$regEnt->setTipo($item->tipo);
			$regEnt->setUsuarioId($item->usuario_id);
			$regEnt->setCorrenteId($item->corrente_id);
			$regEnt->setPoupancaId($item->poupanca_id);
			$regEnt->setCarteiraId($item->carteira_id);
			array_push($listaRegEnt, $regEnt);
		}
		return $listaRegEnt;
	}

	public function balancoMensal($usuarioId, $mes, $ano){
		$connection = new Connection("lib/xuxu.ini");
		$sql = "SELECT valor FROM registroEntrada WHERE usuario_id = :id AND MONTH(data) = :mes AND YEAR(data) = :ano";
		$comando = $connection->getConnection()->prepare($sql);
		$comando->bindParam(":id", $usuarioId);
		$comando->bindParam(":mes", $mes);
		$comando->bindParam(":ano", $ano);
		$comando->execute();
		$resul = $comando->fetchAll();
		$connection->__destruct();
		$valorFinal = 0.0;
		if($resul!=NULL){
			foreach($resul as $item){
				$valorFinal = $valorFinal + $item->valor;
			}
		}
		return $valorFinal;
	}
}
?>
