<?php
    require_once("lib/conection.class.php");
    require_once("src/Modelo/ModeloCorrente.class.php");
    final class ControleCorrente{
        public function criarCorrente($corrente){
            $conexao = new Conection("lib/xuxu.ini");
            $sql = "INSERT INTO corrente (saldo, banco, usuario_id) VALUES (:s,:b,:u)";
            $comando = $conexao->getConection()->prepare($sql);
            $comando->bindValue(":s",$corrente->getSaldo());
            $comando->bindValue(":b",$corrente->getBanco());
            $comando->bindValue(":u",$corrente->getUsuarioId());
            if($comando->execute()){
                $conexao->__destruct();
                return true;
            }else{
                $conexao->__destruct();
                return false;
            }
        }

        public function mostrarTodasCorrentes($usuarioId){
            $conexao = new Conection("lib/xuxu.ini");
            $sql = "SELECT * FROM corrente WHERE usuario_id = :id";
            $comando = $conexao->getConection()->prepare($sql);
            $comando->bindParam(":id", $usuarioId);
            $comando->execute();
            $resu = $comando->fetchAll();
            $lista = array();
            foreach($resu as $item){
                $corrente = new Corrente();
                $corrente->setId($item->id);
                $corrente->setSaldo($item->saldo);
                $corrente->setBanco($item->banco);
                $corrente->setUsuarioId($item->usuario_id);
                array_push($lista, $corrente);
            }
            $conexao->__destruct();
            return $lista;
        }
        public function mostrarCorrente($correnteId){
            $conexao = new Conection("lib/xuxu.ini");
            $sql = "SELECT * FROM corrente WHERE id = :id";
            $comando = $conexao->getConection()->prepare($sql);
            $comando->bindParam(":id", $correnteId);
            $comando->execute();
            $resu = $comando->fetch();
            $corrente = new Corrente();
            $corrente->setId($resu->id);
            $corrente->setSaldo($resu->saldo);
            $corrente->setBanco($resu->banco);
            $corrente->setUsuarioId($resu->usuario_id);
            $conexao->__destruct();
            return $corrente;
        }

        public function adicionarFundos($id, $valor){
            $conexao = new Conection("lib/xuxu.ini");
            $sql = "UPDATE corrente SET saldo = saldo + :v WHERE id=:id;";
            $comando = $conexao->getConection()->prepare($sql);
            $comando->bindParam("v",$valor);
            $comando->bindParam("id",$id);
            if($comando->execute()){
                $conexao->__destruct();
                return true;
            }else{
                $conexao->__destruct();
                return false;
            }
        }

        public function retirarFundos($id, $valor){
            $conexao = new Conection("lib/xuxu.ini");
            $sql = "UPDATE corrente SET saldo = saldo - :v WHERE id=:id;";
            $comando = $conexao->getConection()->prepare($sql);
            $comando->bindParam("v",$valor);
            $comando->bindParam("id",$id);
            if($comando->execute()){
                $conexao->__destruct();
                return true;
            }else{
                $conexao->__destruct();
                return false;
            }
        }

    }

?>