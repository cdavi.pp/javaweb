<?php
    require_once("lib/conection.class.php");
    require_once("src/Modelo/ModeloCarteira.class.php");
    final class ControleCarteira{
        public function criarCarteira($carteira){
            $conexao = new Conection("lib/xuxu.ini");
            $sql = "INSERT INTO carteira (saldo, usuario_id) VALUES (:s,:u)";
            $comando = $conexao->getConection()->prepare($sql);
            $comando->bindValue(":s",$carteira->getSaldo());
            $comando->bindValue(":u",$carteira->getUsuarioId());
            if($comando->execute()){
                $conexao->__destruct();
                return true;
            }else{
                $conexao->__destruct();
                return false;
            }
        }

        public function mostrarInformacoes($usuarioId){
            $conexao = new Conection("lib/xuxu.ini");
            $sql = "SELECT id, saldo FROM carteira WHERE usuario_id=:id";
            $comando = $conexao->getConection()->prepare($sql);
            $comando->bindParam(":id", $usuarioId);
            $comando->execute();
            $resul = $comando->fetch();
            $conexao->__destruct();
            $carteira = NULL;
            if($resul!=NULL){
                $carteira = new Carteira();
                $carteira->setId($resul->id);
                $carteira->setSaldo($resul->saldo);
            }
            return $carteira;
        }

        public function adicionarFundos($usuarioId, $valor){
            $conexao = new Conection("lib/xuxu.ini");
            $sql = "UPDATE carteira SET saldo = saldo + :v WHERE usuario_id=:id;";
            $comando = $conexao->getConection()->prepare($sql);
            $comando->bindParam("v",$valor);
            $comando->bindParam("id",$usuarioId);
            if($comando->execute()){
                $conexao->__destruct();
                return true;
            }else{
                $conexao->__destruct();
                return false;
            }
        }

        public function retirarFundos($usuarioId, $valor){
            $conexao = new Conection("lib/xuxu.ini");
            $sql  = "UPDATE carteira SET saldo= saldo - :v WHERE usuario_id=:id;";
            $comando = $conexao->getConection()->prepare($sql);
            $comando->bindParam("v",$valor);
            $comando->bindParam("id",$usuarioId);
            if($comando->execute()){
                $conexao->__destruct();
                return true;
            }else{
                $conexao->__destruct();
                return false;
            }
        }
    }
?>