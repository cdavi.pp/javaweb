<?php
require_once("src/Modelo/ModeloRegistroSaida.class.php");
require_once("lib/Conection.class.php");
class ControleRegistroSaida{
	public function criarRegistro($regSaida){
		$connection = new Conection("lib/xuxu.ini");
		$sql = "INSERT INTO registroSaida(valor, dataRegistro, dataPagamento, tipo, status, usuario_id, cartao_id, corrente_id, carteira_id) VALUES(:v, :dr, :dp, :t, :s, :ui, :cartao, :corrente, :carteira)";
		$comando = $connection->getConection()->prepare($sql);
		$comando->bindValue(":v", $regSaida->getValor());
		$comando->bindValue(":dr", $regSaida->getDataRegistro());
		$comando->bindValue(":dp", $regSaida->getDataPagamento());
		$comando->bindValue(":t", $regSaida->getTipo());
		$comando->bindValue(":s", $regSaida->getStatus());
		$comando->bindValue(":ui", $regSaida->getUsuarioId());
		$comando->bindValue(":cartao", $regSaida->getCartaoId());
		$comando->bindValue(":corrente", $regSaida->getCorrenteId());
		$comando->bindValue(":carteira", $regSaida->getCarteiraId());
		if($comando->execute()){
			$connection->__destruct();
			return true;
		}else{
			$connection->__destruct();
			return false;
		}
	}

	public function mostrarTodosRegistros($usuarioId){
		$connection = new Conection("lib/xuxu.ini");
		$sql = "SELECT * FROM registroSaida WHERE usuario_id = :id ORDER BY dataRegistro DESC";
		$comando = $connection->getConection()->prepare($sql);
		$comando->bindParam(":id", $usuarioId);
		$comando->execute();
		$resul = $comando->fetchAll();
		$connection->__destruct();
		$listaRegSaida = array();
		foreach($resul as $item){
			$regSaida = new RegistroSaida();
			$regSaida->setId($item->id);
			$regSaida->setValor($item->valor);
			$regSaida->setDataRegistro($item->dataRegistro);
			$regSaida->setDataPagamento($item->dataPagamento);
			$regSaida->setTipo($item->tipo);
			$regSaida->setStatus($item->status);
			$regSaida->setUsuarioId($item->usuario_id);
			$regSaida->setCartaoId($item->cartao_id);
			$regSaida->setCorrenteId($item->corrente_id);
			$regSaida->setCarteiraId($item->carteira_id);
			array_push($listaRegSaida, $regSaida);
		}
		return $listaRegSaida;
	}

	public function mostrarTodosRegistrosCorrente($usuarioId){
		$connection = new Conection("lib/xuxu.ini");
		$sql = "SELECT * FROM registroSaida WHERE usuario_id = :id AND corrente_id <> 'NULL'  ORDER BY dataRegistro DESC";
		$comando = $connection->getConection()->prepare($sql);
		$comando->bindParam(":id", $usuarioId);
		$comando->execute();
		$resul = $comando->fetchAll();
		$connection->__destruct();
		$listaRegSaida = array();
		foreach($resul as $item){
			$regSaida = new RegistroSaida();
			$regSaida->setId($item->id);
			$regSaida->setValor($item->valor);
			$regSaida->setDataRegistro($item->dataRegistro);
			$regSaida->setDataPagamento($item->dataPagamento);
			$regSaida->setTipo($item->tipo);
			$regSaida->setStatus($item->status);
			$regSaida->setUsuarioId($item->usuario_id);
			$regSaida->setCartaoId($item->cartao_id);
			$regSaida->setCorrenteId($item->corrente_id);
			$regSaida->setCarteiraId($item->carteira_id);
			array_push($listaRegSaida, $regSaida);
		}
		return $listaRegSaida;
	}

	public function mostrarTodosRegistrosCartao($usuarioId){
		$connection = new Conection("lib/xuxu.ini");
		$sql = "SELECT * FROM registroSaida WHERE usuario_id = :id AND poupanca_id <> 'NULL'  ORDER BY dataRegistro DESC";
		$comando = $connection->getConection()->prepare($sql);
		$comando->bindParam(":id", $usuarioId);
		$comando->execute();
		$resul = $comando->fetchAll();
		$connection->__destruct();
		$listaRegSaida = array();
		foreach($resul as $item){
			$regSaida = new RegistroSaida();
			$regSaida->setId($item->id);
			$regSaida->setValor($item->valor);
			$regSaida->setDataRegistro($item->dataRegistro);
			$regSaida->setDataPagamento($item->dataPagamento);
			$regSaida->setTipo($item->tipo);
			$regSaida->setStatus($item->status);
			$regSaida->setUsuarioId($item->usuario_id);
			$regSaida->setCartaoId($item->cartao_id);
			$regSaida->setCorrenteId($item->corrente_id);
			$regSaida->setCarteiraId($item->carteira_id);
			array_push($listaRegSaida, $regSaida);
		}
		return $listaRegSaida;
	}

	public function mostrarTodosRegistrosCarteira($usuarioId){
		$connection = new Conection("lib/xuxu.ini");
		$sql = "SELECT * FROM registroSaida WHERE usuario_id = :id AND carteira_id <> 'NULL'  ORDER BY dataRegistro DESC";
		$comando = $connection->getConection()->prepare($sql);
		$comando->bindParam(":id", $usuarioId);
		$comando->execute();
		$resul = $comando->fetchAll();
		$connection->__destruct();
		$listaRegSaida = array();
		foreach($resul as $item){
			$regSaida = new RegistroSaida();
			$regSaida->setId($item->id);
			$regSaida->setValor($item->valor);
			$regSaida->setDataRegistro($item->dataRegistro);
			$regSaida->setDataPagamento($item->dataPagamento);
			$regSaida->setTipo($item->tipo);
			$regSaida->setStatus($item->status);
			$regSaida->setUsuarioId($item->usuario_id);
			$regSaida->setCartaoId($item->cartao_id);
			$regSaida->setCorrenteId($item->corrente_id);
			$regSaida->setCarteiraId($item->carteira_id);
			array_push($listaRegSaida, $regSaida);
		}
		return $listaRegSaida;
	}
	public function mostrarProximosPagamentos($usuarioId){
		$connection = new Conection('lib/xuxu.ini');
		$sql = "SELECT * FROM registroSaida WHERE usuario_id = :id AND status = 2 ORDER BY dataPagamento DESC";
		$comando = $connection->getConection()->prepare($sql);
		$comando->bindParam(":id", $usuarioId);
		$comando->execute();
		$resul = $comando->fetchAll();
		$connection->__destruct();
		$listaRegSaida = array();
		foreach($resul as $item){
			$regSaida = new RegistroSaida();
			$regSaida->setId($item->id);
			$regSaida->setValor($item->valor);
			$regSaida->setDataRegistro($item->dataRegistro);
			$regSaida->setDataPagamento($item->dataPagamento);
			$regSaida->setTipo($item->tipo);
			$regSaida->setStatus($item->status);
			$regSaida->setUsuarioId($item->usuario_id);
			$regSaida->setCartaoId($item->cartao_id);
			$regSaida->setCorrenteId($item->corrente_id);
			$regSaida->setCarteiraId($item->carteira_id);
			array_unshift($listaRegSaida, $regSaida);
		}
		return $listaRegSaida;
	}
	public function pagarRegistro($cartaoId, $data){
		$connection = new Conection("lib/xuxu.ini");
		$sql = "UPDATE registroSaida SET status = 1 WHERE cartao_id=:cartao AND dataPagamento=:data AND status = 2";
		$comando = $connection->getConnection()->prepare($sql);
		$comando->bindParam(":cartao", $cartaoId);
		$comando->bindParam(":data", $data);
		if($comando->execute()){
			$connection->__destruct();
			return true;
		}else{
			$connection->__destruct();
			return false;
		}
	}
	public function balancoMensal($usuarioId, $mes, $ano){
		$connection = new Conection("lib/xuxu.ini");
		$sql = "SELECT valor FROM registroSaida WHERE usuario_id = :id AND MONTH(dataPagamento) = :mes AND YEAR(dataPagamento) = :ano AND status = 1";
		$comando = $connection->getConnection()->prepare($sql);
		$comando->bindParam(":id", $usuarioId);
		$comando->bindParam(":mes", $mes);
		$comando->bindParam(":ano", $ano);
		$comando->execute();
		$resul = $comando->fetchAll();
		$connection->__destruct();
		$valorFinal = 0.0;
		if($resul!=NULL){
			foreach($resul as $item){
				$valorFinal = $valorFinal + $item->valor;
			}
		}
		return $valorFinal;
	}

	public function valorAPagarFatura($usuarioId, $cartaoId, $data){
		$connection = new Conection("lib/xuxu.ini");
		$sql = "SELECT valor FROM registroSaida WHERE usuario_id = :id AND cartao_id = :cartao AND dataPagamento = :data AND status = 2";
		$comando = $connection->getConnection()->prepare($sql);
		$comando->bindParam(":id", $usuarioId);
		$comando->bindParam(":cartao", $cartaoId);
		$comando->bindParam(":data", $data);
		$comando->execute();
		$resul = $comando->fetchAll();
		$connection->__destruct();
		$valorFinal = 0.0;
		foreach($resul as $item){
			$valorFinal = $valorFinal + $item->valor;
		}
		return $valorFinal;
	}
}