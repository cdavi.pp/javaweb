<?php
    require_once("lib/conection.class.php");
    require_once("src/Modelo/ModeloUsuario.class.php");
    final class ControleUsuario{
        public function criarUsuario($usuario){
            $conexao = new Conection("lib/xuxu.ini");
            $sql = "INSERT INTO usuario (nome, email, senha) VALUES (:n,:e,:s)";
            $comando = $conexao->getConection()->prepare($sql);
            $comando->bindValue(":n",$usuario->getNome());
            $comando->bindValue(":e",$usuario->getEmail());
            $comando->bindValue(":s",$usuario->getSenha());
            if($comando->execute()){
                $conexao->__destruct();
                return true;
            }else{
                $conexao->__destruct();
                return false;
            }
        }

        public function logarUsuario($usuario){
            $conexao = new Conection("lib/xuxu.ini");
            $sql = "SELECT id, count(id) as c FROM usuario WHERE email=:e AND senha=:s";
            $comando = $conexao->getConection()->prepare($sql);
            $comando->bindValue(":e",$usuario->getEmail());
            $comando->bindValue(":s",$usuario->getSenha());
            $comando->execute();
            $resu = $comando->fetch();
            $conexao->__destruct();
            if(($resu->c)!=0){                
                return $resu->id;
            }else{
                return false;
            }
        }

        public function alterarSenha($usuario){
            $conexao = new Conection("lib/xuxu.ini");
            $sql = "UPDATE usuario SET senha=:s WHERE id=:id";
            $comando = $conexao->getConection()->prepare($sql);
            $comando->bindValue(":id",$usuario->getId());
            $comando->bindValue(":s",$usuario->getSenha());
            if($comando->execute()){
                $conexao->__destruct();
                return true;
            }else{
                $conexao->__destruct();
                return false;
            }
        }
        public function verificarEmail($email){
            $conexao = new Conection("lib/xuxu.ini");
            $sql = "SELECT count(id) AS c FROM usuario WHERE email = :e";
            $comando = $conexao->getConection()->prepare($sql);
            $comando->bindParam(":e", $email);
            $comando->execute();
            $resu = $comando->fetch();
            $conexao->__destruct();
            if($resu->c==0){
                return true;
            }else{
                return false;
            }
        }
        public function mostrarInformacoes($usuarioId){
            $conexao = new Conection("lib/xuxu.ini");
            $sql = "SELECT nome, email FROM usuario WHERE id = :id";
            $comando = $conexao->getConection()->prepare($sql);
            $comando->bindParam(":id", $usuarioId);
            $comando->execute();
            $resul = $comando->fetch();
            $usuario = new Usuario();
            $usuario->setNome($resul->nome);
            $usuario->setEmail($resul->email);
            return $usuario;
        }
    }
?>