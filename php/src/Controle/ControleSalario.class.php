<?php
    require_once("lib/Conection.class.php");
    require_once("src/Modelo/ModeloSalario.class.php");
    class ControleSalario{
        public function CriarSalario($salario){
            $conexao = new Conection("lib/xuxu.ini");
            $sql = "INSERT INTO salario(valor, data, usuario_id) VALUES(:va, :da, :idu)";
            $comando = $conexao->getConection()->prepare($sql);
            $comando->bindValue(":va",$salario->getValor());
            $comando->bindValue(":da",$salario->getData());
            $comando->bindValue(":idu",$salario->getUsuarioId());
            if($comando->execute()){
                $conexao->__destruct();
                return true;
            }else{
                $conexao->__destruct();
                return false;
            }
        }
        public function Informacao($id){
            $conexao = new Conection("lib/xuxu.ini");
            $sql = "SELECT * FROM salario WHERE usuario_id = :id";
            $comando = $conexao->getConection()->prepare($sql);
            $comando->bindParam(":id", $id);
            $comando->execute();
            $resu = $comando->fetch();
            if($resu!=NULL){
                $salario = new Salario();
                $salario->setId($resu->id);
                $salario->setValor($resu->valor);
                $salario->setData($resu->data);
                $salario->setUsuarioId($resu->usuario_id);
                $conexao->__destruct();
                return $salario;
            }else{
                return NULL;
            }
        }
        public function AlterarInformacoes($salario){
            $conexao = new Conection("lib/xuxu.ini");
            $sql = "UPDATE salario SET valor=:va, data = :data WHERE usuario_id=:id;";
            $comando = $conexao->getConection()->prepare($sql);
            $comando->bindValue(":va",$salario->getValor());
            $comando->bindValue(":data", $salario->getData());
            $comando->bindValue(":id", $salario->getUsuarioId());
            if($comando->execute()){
                $conexao->__destruct();
                return true;
            }else{
                $conexao->__destruct();
                return false;
            }
        }
        public function EntradaMensal($usuarioId){
            //alterar data;
            $conexao = new Conection("lib/xuxu.ini");
            $sql = "UPDATE salario SET data= ADDDATE(data, INTERVAL 1 MONTH) WHERE usuario_id=:id;";
            $comando = $conexao->getConection()->prepare($sql);
            $comando->bindValue(":id", $usuarioId);
            if($comando->execute()){
                $conexao->__destruct();
                return true;
            }else{
                $conexao->__destruct();
                return false;
            }
        }
    }
?>