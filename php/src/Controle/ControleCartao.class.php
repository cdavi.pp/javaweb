<?php
require_once("src/Modelo/ModeloCartao.class.php");
require_once("lib/Conection.class.php");
class ControleCartao{
	public function adicionarCartao($cartao){
		$connection = new Conection("lib/xuxu.ini");
		$sql = "INSERT INTO cartao(numero, limiteVista, limitePrazo, melhorDia, tipo, dataVencimento, usuario_id, corrente_id) VALUES(:n, :lv, :lp, :md, :t, :dv, :ui, :ci)";
		$comando = $connection->getConection()->prepare($sql);
		$comando->bindValue(":n", $cartao->getNumero());
		$comando->bindValue(":lv", $cartao->getLimiteVista());
		$comando->bindValue(":lp", $cartao->getLimitePrazo());
		$comando->bindValue(":md", $cartao->getMelhorDia());
		$comando->bindValue(":t", $cartao->getTipo());
		$comando->bindValue(":dv", $cartao->getDataVencimento());
		$comando->bindValue(":ui", $cartao->getUsuarioId());
		$comando->bindValue(":ci", $cartao->getCorrenteId());
		if($comando->execute()){
			$connection->__destruct();
			return true;
		}else{
			$connection->__destruct();
			return false;
		}
	}

	public function mostrarTodosCartoes($usuarioId){
		$connection  = new Conection("lib/xuxu.ini");
		$sql = "SELECT * FROM cartao WHERE usuario_id = :id";
		$comando = $connection->getConection()->prepare($sql);
		$comando->bindParam(":id", $usuarioId);
		$comando->execute();
		$resul = $comando->fetchAll();
		$connection->__destruct();
		$listaCartoes = array();
		if($resul!=NULL){
			foreach($resul as $item){
				$cartao = new Cartao();
				$cartao->setId($item->id);
				$cartao->setNumero($item->numero);
				$cartao->setLimiteVista($item->limiteVista);
				$cartao->setLimitePrazo($item->limitePrazo);
				$cartao->setMelhorDia($item->melhorDia);
				$cartao->setTipo($item->tipo);
				$cartao->setDataVencimento($item->dataVencimento);
				$cartao->setUsuarioId($item->usuario_id);
				$cartao->setCorrenteId($item->corrente_id);
				array_push($listaCartoes, $cartao);
			}
		}
		return $listaCartoes;
	}

	public function mostrarCartoesDebito($usuarioId){
		$connection  = new Conection("lib/xuxu.ini");
		$sql = "SELECT * FROM cartao WHERE usuario_id = :id AND tipo = 'debito' OR tipo = 'credeb'";
		$comando = $connection->getConection()->prepare($sql);
		$comando->bindParam(":id", $usuarioId);
		$comando->execute();
		$resul = $comando->fetchAll();
		$connection->__destruct();
		$listaCartoes = array();
		if($resul!=NULL){
			foreach($resul as $item){
				$cartao = new Cartao();
				$cartao->setId($item->id);
				$cartao->setNumero($item->numero);
				$cartao->setLimiteVista($item->limiteVista);
				$cartao->setLimitePrazo($item->limitePrazo);
				$cartao->setMelhorDia($item->melhorDia);
				$cartao->setTipo($item->tipo);
				$cartao->setDataVencimento($item->dataVencimento);
				$cartao->setUsuarioId($item->usuario_id);
				$cartao->setCorrenteId($item->corrente_id);
				array_push($listaCartoes, $cartao);
			}
		}
		return $listaCartoes;
	}

	public function mostrarCartoesCredito($usuarioId){
		$connection  = new Conection("lib/xuxu.ini");
		$sql = "SELECT * FROM cartao WHERE usuario_id = :id AND tipo = 'credito' OR tipo = 'credeb'";
		$comando = $connection->getConection()->prepare($sql);
		$comando->bindParam(":id", $usuarioId);
		$comando->execute();
		$resul = $comando->fetchAll();
		$connection->__destruct();
		$listaCartoes = array();
		if($resul!=NULL){
			foreach($resul as $item){
				$cartao = new Cartao();
				$cartao->setId($item->id);
				$cartao->setNumero($item->numero);
				$cartao->setLimiteVista($item->limiteVista);
				$cartao->setLimitePrazo($item->limitePrazo);
				$cartao->setMelhorDia($item->melhorDia);
				$cartao->setTipo($item->tipo);
				$cartao->setDataVencimento($item->dataVencimento);
				$cartao->setUsuarioId($item->usuario_id);
				$cartao->setCorrenteId($item->corrente_id);
				array_push($listaCartoes, $cartao);
			}
		}
		return $listaCartoes;	
	}

	public function mostrarCartao($cartaoId){
		$connection  = new Conection("lib/xuxu.ini");
		$sql = "SELECT * FROM cartao WHERE id = :id";
		$comando = $connection->getConection()->prepare($sql);
		$comando->bindParam(":id", $cartaoId);
		$comando->execute();
		$resul = $comando->fetch();
		$connection->__destruct();
		if($resul!=NULL){
			$cartao = new Cartao();
			$cartao->setId($resul->id);
			$cartao->setNumero($resul->numero);
			$cartao->setLimiteVista($resul->limiteVista);
			$cartao->setLimitePrazo($resul->limitePrazo);
			$cartao->setMelhorDia($resul->melhorDia);
			$cartao->setTipo($resul->tipo);
			$cartao->setDataVencimento($resul->dataVencimento);
			$cartao->setUsuarioId($resul->usuario_id);
			$cartao->setCorrenteId($resul->corrente_id);
			return $cartao;
		}else{
			return NULL;
		}
	}
}
?>