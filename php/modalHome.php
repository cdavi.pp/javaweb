<!--Modal para mostrar formulário de criação da carteira -->
<div class="modal fade" id="modalCarteira" tabindex="-1" role="dialog" aria-labelledby="exampleModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title w-100 font-weight-bold" id="exampleModalTitle">Criar Carteira</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="criarCarteira.php" method='POST' id='carteira'>
                <div class="modal-body mx-3">
                    <div class="form-group">
                        <label for="recipient-saldo" class="col-from-label">Valor Já Armazenado:</label>
                        <input type="number" name='saldo' step='.01' class="form-control" id="recipient-saldo" placeholder="Ex.: 1000,99" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" data-dismiss="modal" type="button">Cancelar</button>
                    <input type='submit' class="btn btn-primary" type="button" value='Criar'>
                </div>
            </form>
        </div>
    </div>
</div>

<!--Modal para mostrar formulário de alteração de senha -->
<div class="modal fade" id="modalSenha" tabindex="-1" role="dialog" aria-labelledby="exampleModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title w-100 font-weight-bold" id="exampleModalTitle">Alterar Senha</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="alterarSenha.php" method='POST' id='senha'>
                <div class="modal-body mx-3">
                    <div class="form-group">
                        <label for="recipient-senha" class="col-from-label">Senha nova</label>
                        <input type="password" name='senha'  class="form-control" id="recipient-senha" placeholder="Senha" required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-confirmarSenha" class="col-from-label">Confirmar Senha: </label>
                        <input type="password" name='confirmarSenha'  class="form-control" id="recipient-confirmarSenha" placeholder="Confirmar Senha" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" data-dismiss="modal" type="button">Cancelar</button>
                    <input type='submit' class="btn btn-primary" type="button" value='Criar'>
                </div>
            </form>
        </div>
    </div>
</div>

<!--Modal para mostrar formulário de criação de salário -->
<div class="modal fade" id="modalSalario" tabindex="-1" role="dialog" aria-labelledby="exampleModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title w-100 font-weight-bold" id="exampleModalTitle">Criar Salário</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="criarSalario.php" method='POST' id='salario'>
                <div class="modal-body mx-3">
                    <div class="form-group">
                        <label for="recipient-valor" class="col-from-label">Valor do Salário: </label>
                        <input type="number" name='valor' step='.01' class="form-control" id="recipient-valor" placeholder="Ex.: 1000,99" required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-data" class="col-from-label">Data do próximo pagamento: </label>
                        <input type="date" name='data' class="form-control" id="recipient-data" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" data-dismiss="modal" type="button">Cancelar</button>
                    <input type='submit' class="btn btn-primary" type="button" value='Criar'>
                </div>
            </form>
        </div>
    </div>
</div>

<!--Modal para mostrar formulário de alteração do salário -->
<?php if($salario!=NULL){
echo '
<div class="modal fade" id="modalEditarSalario" tabindex="-1" role="dialog" aria-labelledby="exampleModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title w-100 font-weight-bold" id="exampleModalTitle">Editar Salário</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="alterarSalario.php" method="POST" id="editarSalario">
                <div class="modal-body mx-3">
                    <div class="form-group">
                        <label for="recipient-valor" class="col-from-label">Valor do Salário: </label>
                        <input type="number" name="valor" step=".01" class="form-control" id="recipient-valor" placeholder="Ex.: 1000,99" value="' . $salario->getValor() . '"required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-data" class="col-from-label">Data do próximo pagamento: </label>
                        <input type="date" name="data" class="form-control" id="recipient-data" value="' . $salario->getData() . '" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" data-dismiss="modal" type="button">Cancelar</button>
                    <input type="submit" class="btn btn-primary" type="button" value="Criar">
                </div>
            </form>
        </div>
    </div>
</div>
';
}
?>

<!--Modal para avisar que houve êxito no processo de criação de carteira -->
<div class="modal fade" id="sucessoCarteira" tabindex="-1" role="dialog" aria-labelledby="exampleModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title w-100 font-weight-bold" id="exampleModalTitle">Sucesso</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <img src="img/success.png" width="30%" height="100%" alt="">
                Sua carteira foi criada com sucesso!
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" data-dismiss="modal" type="button">Ok</button>
            </div>
        </div>
    </div>
</div>

<!--Modal para avisar que houve êxito no processo de criação de carteira -->
<div class="modal fade" id="sucessoSalario" tabindex="-1" role="dialog" aria-labelledby="exampleModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title w-100 font-weight-bold" id="exampleModalTitle">Sucesso</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <img src="img/success.png" width="30%" height="100%" alt="">
                Seu salário foi criado com sucesso
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" data-dismiss="modal" type="button">Ok</button>
            </div>
        </div>
    </div>
</div>

<!--Modal para avisar que houve êxito no processo de alterar salario -->
<div class="modal fade" id="sucessoEditarSalario" tabindex="-1" role="dialog" aria-labelledby="exampleModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title w-100 font-weight-bold" id="exampleModalTitle">Sucesso</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <img src="img/success.png" width="30%" height="100%" alt="">
                Seu salário foi alterado com sucesso
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" data-dismiss="modal" type="button">Ok</button>
            </div>
        </div>
    </div>
</div>

<!--Modal para avisar que houve um erro na aplicação -->
<div class="modal fade" id="erroGeral" tabindex="-1" role="dialog" aria-labelledby="exampleModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title w-100 font-weight-bold" id="exampleModalTitle">Algo deu errado :(</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <img src="img/error.png" width="30%" height="100%" alt="">
                Ocorreu um erro, atualize a página e tente novamente.
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" data-dismiss="modal" type="button">Ok</button>
            </div>
        </div>
    </div>
</div>

<!--Modal para avisar que houve erro no processo de alteração de senha (as senhas não coincidem) -->
<div class="modal fade" id="erroSenhaDiferente" tabindex="-1" role="dialog" aria-labelledby="exampleModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title w-100 font-weight-bold" id="exampleModalTitle">Algo deu errado :(</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <img src="img/error.png" width="30%" height="100%" alt="">
                Senhas diferentes, tente novamente.
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" data-dismiss="modal" type="button">Ok</button>
            </div>
        </div>
    </div>
</div>

<!--Modal para avisar que houve êxito no processo de alterar a senha -->
<div class="modal fade" id="sucessoSenha" tabindex="-1" role="dialog" aria-labelledby="exampleModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title w-100 font-weight-bold" id="exampleModalTitle">Sucesso</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <img src="img/success.png" width="30%" height="100%" alt="">
                Sua senha foi alterada com sucesso
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" data-dismiss="modal" type="button">Ok</button>
            </div>
        </div>
    </div>
</div>

<!--Modal para mostrar formulário de criação de poupança -->
<div class="modal fade" id="modalPoupanca" tabindex="-1" role="dialog" aria-labelledby="exampleModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title w-100 font-weight-bold" id="exampleModalTitle">Criar Conta Poupança</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="criarPoupanca.php" method='POST' id='poupanca'>
                <div class="modal-body mx-3">
                    <div class="form-group">
                        <label for="recipient-saldo" class="col-from-label">Valor Já Armazenado:</label>
                        <input type="number" name='saldo' step='.01' class="form-control" id="recipient-saldo" placeholder="Ex.: 1000,99" required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-data" class="col-from-label">Data da próxima adição de taxa: </label>
                        <input type="date" name='data' class="form-control" id="recipient-data" required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-banco" class="col-from-label">Banco da Conta: </label>
                        <input type="text" name='banco' class="form-control" id="recipient-banco" placeholder="Ex.: Banco do Brasil" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" data-dismiss="modal" type="button">Cancelar</button>
                    <input type='submit' class="btn btn-primary" type="button" value='Criar'>
                </div>
            </form>
        </div>
    </div>
</div>

<!--Modal para avisar que houve êxito no processo de criar poupança -->
<div class="modal fade" id="sucessoPoupanca" tabindex="-1" role="dialog" aria-labelledby="exampleModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title w-100 font-weight-bold" id="exampleModalTitle">Sucesso</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <img src="img/success.png" width="30%" height="100%" alt="">
                Sua conta poupança foi criada!
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" data-dismiss="modal" type="button">Ok</button>
            </div>
        </div>
    </div>
</div>

<!--Modal para mostrar formulário de criação de corrente -->
<div class="modal fade" id="modalCorrente" tabindex="-1" role="dialog" aria-labelledby="exampleModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title w-100 font-weight-bold" id="exampleModalTitle">Criar Conta Corrente</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="criarCorrente.php" method='POST' id='corrente'>
                <div class="modal-body mx-3">
                    <div class="form-group">
                        <label for="recipient-saldo" class="col-from-label">Valor Já Armazenado:</label>
                        <input type="number" name='saldo' step='.01' class="form-control" id="recipient-saldo" placeholder="Ex.: 1000,99" required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-banco" class="col-from-label">Banco da Conta: </label>
                        <input type="text" name='banco' class="form-control" id="recipient-banco" placeholder="Ex.: Banco do Brasil" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" data-dismiss="modal" type="button">Cancelar</button>
                    <input type='submit' class="btn btn-primary" type="button" value='Criar'>
                </div>
            </form>
        </div>
    </div>
</div>

<!--Modal para avisar que houve êxito no processo de criar corrente -->
<div class="modal fade" id="sucessoCorrente" tabindex="-1" role="dialog" aria-labelledby="exampleModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title w-100 font-weight-bold" id="exampleModalTitle">Sucesso</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <img src="img/success.png" width="30%" height="100%" alt="">
                Sua conta corrente foi criada!
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" data-dismiss="modal" type="button">Ok</button>
            </div>
        </div>
    </div>
</div>

<!--Modal para mostrar formulário de criação de cartao -->
<div class="modal fade" id="modalCartao" tabindex="-1" role="dialog" aria-labelledby="exampleModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title w-100 font-weight-bold" id="exampleModalTitle">Criar Cartão</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="criarCartao.php" method='POST' id='cartao'>
                <div class="modal-body mx-3">
                    <div class="form-group">
                        <label for="recipient-numero" class="col-from-label">Numero do Cartao</label>
                        <input type="number" name='numero' class="form-control" maxlength="16" minlength="16" id="recipient-numero" placeholder="Ex.: 8888777766665555" required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-tipo">Tipo de Cartão</label>
                        <select class="form-control" name='tipo' id="recipient-tipo" required>
                            <option selected disabled>Selecione um tipo de Cartão</option>
                            <option value='credito'>Crédito</option>
                            <?php
                            if($contasCorrente!=null){ ?>
                                <option value='credeb'>Crédito e Débito</option>
                                <option value='debito'>Débito</option>
                            <?php
                            }else{?>
                                <option disabled>Para a opção débito, crie uma corrente antes</option>
                            <?php }
                            ?>
                        </select>
                    </div>
                    <div id='ajaxCartao'></div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" data-dismiss="modal" type="button">Cancelar</button>
                    <input type='submit' class="btn btn-primary" type="button" value='Criar'>
                </div>
            </form>
        </div>
    </div>
</div>

<!--Modal para avisar que houve êxito no processo de criar corrente -->
<div class="modal fade" id="sucessoCartao" tabindex="-1" role="dialog" aria-labelledby="exampleModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title w-100 font-weight-bold" id="exampleModalTitle">Sucesso</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <img src="img/success.png" width="30%" height="100%" alt="">
                Seu cartão foi criado!
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" data-dismiss="modal" type="button">Ok</button>
            </div>
        </div>
    </div>
</div>