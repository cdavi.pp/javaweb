<?php
    require_once("src/Modelo/ModeloUsuario.class.php");
    require_once("src/Controle/ControleUsuario.class.php");
    $usuario = new Usuario();
    $controle = new ControleUsuario();
    if($controle->verificarEmail($_POST['email'])){
        $usuario->setNome($_POST['nome']);
        $usuario->setEmail($_POST['email']);
        $senha = $_POST['senha'];
        $salt = "bJdpGaBSYRrgRStF";
        for($a=0;$a<640;$a++){
            $salt = md5($salt);    
        }
        $crypt = crypt($senha, $salt);
        $usuario->setSenha($crypt);
        if($controle->criarUsuario($usuario)){
            echo 1;
        }else{
            echo 0;
        }
    }else{
        echo 2;
    }
?>