<div class="form-group">
	<label for="recipient-correnteId">Conta Corrente: </label>
	<select class="form-control" name='correnteId' id="recipient-correnteId" required>
		<option>Escolha uma opção</option>
		<?php 
			require_once("src/Controle/ControleCorrente.class.php");
			session_start();
			$controleCorrente = new ControleCorrente();
			$contasCorrente = $controleCorrente->mostrarTodasCorrentes($_SESSION['id']);	
			foreach($contasCorrente as $item){
				echo "<option value=" . $item->getId() . ">" . $item->getBanco() . "</option>";
			}
		?>
	</select>
</div>