$('#recipient-forma').change(function() {
    if ($(this).val() === 'carteira') {
        requisitarArquivo("ajaxCarteira.php", ajaxSaida);
    }else if ($(this).val() === 'credito'){
    	requisitarArquivo("ajaxCartoesCredito.php", ajaxSaida);
   	}else if ($(this).val() === 'debito'){
   		requisitarArquivo("ajaxCartoesDebito.php", ajaxSaida);
   	}
});
$('#recipient-tipo').change(function() {
    if ($(this).val() === 'outroTipo') {
    	requisitarArquivo("ajaxOutroTipo.html", ajaxOutroTipo);
    }
});