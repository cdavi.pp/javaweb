
<!--Modal para mostrar formulário de criação de um registro de entrada -->
<div class="modal fade" id="modalRegistroEntrada" tabindex="-1" role="dialog" aria-labelledby="exampleModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title w-100 font-weight-bold" id="exampleModalTitle">Novo Registro de Entrada</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="criarRegistroEntrada.php" method='POST' id='registroEntrada'>
                <div class="modal-body mx-3">
                    <div class="form-group">
                        <label for="recipient-valor" class="col-from-label">Valor: </label>
                        <input type="number" name='valor' step='.01' class="form-control" id="recipient-valor" placeholder="Ex.: 1000,99" required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-data" class="col-from-label">Data do Registro: </label>
                        <input type="date" name='data' class="form-control" id="recipient-data" required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-tipo">Tipo da Entrada: </label>
                        <select class="form-control" name='tipo' id="recipient-tipo" required>
                            <option>Escolha uma opção</option>
                            <option value="Herança">Herança</option>
                            <option value="Investimentos Financeiros">Investimentos Financeiros</option>
                            <option value="Doação">Doação</option>
                            <option value="Empréstimo">Empréstimo</option>
                            <option value="outroTipo">Outro...</option>
                        </select>
                    </div>
                    <div id='ajaxOutroTipo'></div>
                    <div class="form-group">
                        <label for="recipient-forma">Local da Entrada: </label>
                        <select class="form-control" name='forma' id="recipient-forma" required>
                            <option>Escolha uma opção</option>
                            <option value="carteira">Carteira</option>
                            <option value="poupanca">Conta Poupança</option>
                            <option value="corrente">Conta Corrente</option>
                        </select>
                    </div>
                    <div id='ajaxEntrada'></div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" data-dismiss="modal" type="button">Cancelar</button>
                    <input type='submit' class="btn btn-primary" type="button" value='Criar'>
                </div>
            </form>
        </div>
    </div>
</div>

<!--Modal para avisar que houve um erro na aplicação -->
<div class="modal fade" id="erroGeral" tabindex="-1" role="dialog" aria-labelledby="exampleModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title w-100 font-weight-bold" id="exampleModalTitle">Algo deu errado :(</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <img src="img/error.png" width="30%" height="100%" alt="">
                Ocorreu um erro, atualize a página e tente novamente.
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" data-dismiss="modal" type="button">Ok</button>
            </div>
        </div>
    </div>
</div>

<!--Modal para avisar que houve êxito no processo de criar registro -->
<div class="modal fade" id="sucessoRegistroEntrada" tabindex="-1" role="dialog" aria-labelledby="exampleModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title w-100 font-weight-bold" id="exampleModalTitle">Sucesso</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <img src="img/success.png" width="30%" height="100%" alt="">
                Seu registro foi criado.
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" data-dismiss="modal" type="button">Ok</button>
            </div>
        </div>
    </div>
</div>