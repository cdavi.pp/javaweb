<?php
    require_once("src/Modelo/ModeloUsuario.class.php");
    require_once("src/Controle/ControleUsuario.class.php");
    session_start();
    $usuario = new Usuario();
    $controle = new ControleUsuario();
    if($_POST['senha'] == $_POST['confirmarSenha']){
        $senha = $_POST['senha'];
        $salt = "bJdpGaBSYRrgRStF";
        for($a=0;$a<640;$a++){
            $salt = md5($salt);    
        }
        $crypt = crypt($senha, $salt);
        $usuario->setSenha($crypt);
        $usuario->setId($_SESSION['id']);
        if($controle->alterarSenha($usuario)){
            echo 1;
        }else{
            echo 0;
        }
    }else{
        echo 2;
    }
?>