<div class="form-group">
	<label for="recipient-cartaoId">Cartão: </label>
	<select class="form-control" name='cartaoId' id="recipient-cartaoId" required>
		<option selected disabled>Escolha um cartão</option>
		<?php 
			require_once("src/Controle/ControleCartao.class.php");
			require_once("src/Controle/ControleCorrente.class.php");
			session_start();
			$controleCartao = new ControleCartao();
			$cartoes = $controleCartao->mostrarCartoesCredito($_SESSION['id']);
			$controleCorrente = new ControleCorrente();
			foreach($cartoes as $item){
				echo "<option value=" . $item->getId() . ">Número do Cartão: " . $item->getNumero() . "</option>";
			}
		?>
	</select>
</div>
<div class="form-group">
	<label for="vezes">Número de Vezes</label>
	<input type='number' name='vezes' class='form-control' id='vezes'>
</div>