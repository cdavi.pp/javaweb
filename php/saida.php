<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Registros de Saída</title>
        <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.min.css">
    </head>
    <body>
        <header>
            <?php require_once("navBar.php"); ?>
            <section class="jumbotron text-center">
                <div class="container">
                    <h1 class="jumbotron-heading">Registros de Saída</h1>
                    <p class="lead text-muted">Todas as saídas de dinheiro são registradas e essas informações são mostradas nesta página</a></p>
                    <div id="funcionalidades"></div>
                </div>
            </section>
        </header>
        <main role="main">
            <div class="content container marketing">
                <center><h2>Saídas</h2></center>
                <hr>
                <center>
                    <button type='button' class='btn btn-primary' data-toggle='modal' data-target='#modalRegistroSaida'>Criar um novo registro</button>
                </center>
                <hr>
                <table class="table table-striped table-dark">
                    <thead>
                        <tr>
                            <th scope="col">Data do Registro</th>
                            <th scope="col">Data do Pagamento</th>
                            <th scope="col">Valor</th>
                            <th scope="col">Tipo</th>
                            <th scope="col">Método de Pagamento</th>
                            <th scope="col">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                    require_once("src/Controle/ControleRegistroSaida.class.php");
                    require_once("src/Controle/ControleCorrente.class.php");
                    require_once("src/Controle/ControleCartao.class.php");
                    $controleSaida = new ControleRegistroSaida();
                    $controleCorrente = new ControleCorrente();
                    $controleCartao = new ControleCartao();
                    $registrosSaida = $controleSaida->mostrarTodosRegistros($_SESSION['id']);
                    if($registrosSaida != NULL);
                    foreach($registrosSaida as $item){
                        $dataRegistro = new DateTime($item->getDataRegistro());
                        $dataPagamento = new DateTime($item->getDataPagamento());
                        echo "
                        <tr>
                            <td>" . $dataRegistro->format('d/m/Y') . "</td>
                            <td>" . $dataPagamento->format('d/m/Y') . "</td>
                            <td>" . $item->getValor() . "</td>
                            <td>" . $item->getTipo() . "</td>
                            <td>";
                            if($item->getCarteiraId() != NULL){
                                echo "Carteira";
                            }else if($item->getCorrenteId()!=NULL){
                                $contaCorrente = $controleCorrente->mostrarCorrente($item->getCorrenteId());
                                echo "Corrente - " . $contaCorrente->getBanco();
                            }else if($item->getCartaoId()!=NULL){
                                $cartao = $controleCartao->mostrarCartao($item->getCartaoId());
                                $contaCorrente = $controleCorrente->mostrarCorrente($cartao->getCorrenteId());
                                echo "Cartao - " . $contaCorrente->getBanco();
                            }
                            echo 
                            "</td>
                            <td>";
                            if($item->getStatus()==1){
                                echo "Pago.";
                            }else{
                                echo "Não pago.";
                            }
                            echo 
                            "</td>
                        </tr>";
                    }
                    ?>
                    </tbody>
                </table>
                <br>
            </div>
            <?php 
            require_once("footer.php");
            require_once("modalSaida.php");
            ?>
        </main>
    </body>
    <script src='js/jquery.js'></script>
    <script src="bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>
    <script src="bootstrap-4.3.1-dist/js/bootstrap.bundle.js"></script>
    <script src="js/ajax.js"></script>
    <script src="js/saidaAjax.js"></script>
    <script>
        $("#registroSaida").submit(function(){
            var dadosSaida = $(this).serialize();
            $.post("criarRegistroSaida.php", dadosSaida, function(resultado){
                if(resultado == 0){
                    $("#erroGeral").modal("show");
                }else if(resultado == 1){
                    $("#modalRegistroSaida").modal("hide");
                    $("#sucessoRegistroSaida").modal("show");
                }
            });
            return false;
        });
        $("#sucessoRegistroSaida").on("hide.bs.modal", function(){
            window.location.replace("saida.php");
        });
    </script>
</html>