<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Home</title>
        <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.min.css">
    </head>
    <style>
        .corS{
            color: #BEBEBE;
        }
        .corP{
            color: #E6E6FA;
        }
        .corU{
            color: white;
        }
    </style>
    <body>
        <header>
            <?php require_once("navBar.php"); ?>
            <section class="jumbotron text-center">
                <div class="container">
                    <h1 class="jumbotron-heading"><span class="text-muted">X</span>Finanças</h1>
                    <p class="lead text-muted">As tabelas mostram suas informações. Caso elas forem editadas, você acompanha as mudanças simultaneamente. Também pode ver contas com a data próxima para pagar, entre outras funcionalidades.</p>
                    <div id="funcionalidades"></div>
                </div>
            </section>
        </header>
            <main role="main">
                <div class="content container marketing">
                    <main role="main">
                        <div class="jumbotron corU bg-dark">
                            <div class="col-sm-10 mx-auto">
                                <?php
                                    require_once("src/Controle/ControleUsuario.class.php");
                                    require_once("src/Controle/ControleCarteira.class.php");
                                    require_once("src/Controle/ControleSalario.class.php");
                                    require_once("src/Controle/ControleRegistroEntrada.class.php");
                                    require_once("src/Modelo/ModeloRegistroEntrada.class.php");
                                    date_default_timezone_set('America/Sao_Paulo');
                                    //Informações do Usuário
                                    $controleUsuario = new ControleUsuario();
                                    $usuario = $controleUsuario->mostrarInformacoes($_SESSION['id']);
                                    echo "
                                    <h3><span class='corP'>Usuário: </span>" . $usuario->getNome() . "</h3>
                                    <h5><span class='corP'>Email: </span>". $usuario->getEmail() . " <button type='button' class='btn btn-primary' data-toggle='modal' data-target='#modalSenha'>Alterar Senha</button></h5>";

                                    //Informações da carteira
                                    $controleCarteira = new ControleCarteira();
                                    $carteira = $controleCarteira->mostrarInformacoes($_SESSION['id']);                            
                                    if($carteira != NULL){
                                        echo "<h5><span class='corP'>Saldo da Carteira: </span>". $carteira->getSaldo() . "</h5>";
                                    }else{
                                        echo "<button type='button' class='btn btn-primary' data-toggle='modal' data-target='#modalCarteira'>Criar Carteira</button><br><br>";
                                    }
                                    //Instâncias de RegistroEntrada
                                    $controleEntrada = new ControleRegistroEntrada();
                                    $entrada = new RegistroEntrada();
                                    
                                    //Verificação de Entrada Mensal do Salário
                                    $controleSalario = new ControleSalario();
                                    $salario = $controleSalario->Informacao($_SESSION['id']);
                                    if($salario!=NULL){
                                        $verificacao = 0;
                                        $dataSalario = $salario->getData();
                                        $dataAtual = strtotime(date("Y-m-d"));
                                        $diferencaDatas = $dataAtual - strtotime($dataSalario);
                                        if($diferencaDatas>=0){
                                            while($verificacao == 0){
                                                $diferencaDatas = $dataAtual - strtotime($dataSalario);
                                                if($diferencaDatas>=0){
                                                    $entrada->setData($dataSalario); //Preenchimento da Data do Registro Entrada (Entrada do Salário) antes que ocorra uma mudança dela.
                                                    $dataSalarioTime = strtotime($dataSalario.' + 1 month');
                                                    $dataSalario = date('Y-m-d', $dataSalarioTime);
                                                    //Atualizando a data do próximo salário
                                                    $controleSalario->EntradaMensal($_SESSION['id']);
                                                    //Adicionando o valor do Salário na carteira
                                                    $controleCarteira->adicionarFundos($_SESSION['id'], $salario->getValor());
                                                    //Preenchimento dos outros campos de Registro Entrada
                                                    $entrada->setValor($salario->getValor());
                                                    $entrada->setTipo("Salario");
                                                    $entrada->setUsuarioId($_SESSION['id']);
                                                    $entrada->setCarteiraId($carteira->getId());
                                                    $controleEntrada->criarRegistro($entrada);
                                                }else{
                                                    $verificacao = 1;
                                                }
                                            }
                                        }
                                        //Informações do Salário
                                        $dataPag = new DateTime($salario->getData());
                                        echo "<h5><span class='corP'>Valor do Salário: </span>" . $salario->getValor() . " | <span class='corP'>Data do próx. pagamento: </span>" . $dataPag->format('d/m/Y') .
                                         " | <button type='button' class='btn btn-primary' data-toggle='modal' data-target='#modalEditarSalario'>Alterar Salario</button></h5>";
                                    }else if($carteira != NULL){ //Caso o Salario ainda não tenha sido criado, mostra um botão para criar.
                                        echo "<button type='button' class='btn btn-primary' data-toggle='modal' data-target='#modalSalario'>Criar Salário</button>";
                                    }
                                ?>
                                
                            </div>
                        </div>
                    </main>
                    <hr>
                    <center><h2><span class="text-muted">Contas Poupança</span></h2></center>
                    <hr>
                    <!--Card Cartão -->
                    <center>
                        <button type='button' class='btn btn-primary' data-toggle='modal' data-target='#modalPoupanca'>Criar uma Conta Poupança</button>
                    </center>
                    <br>
                    <div class="card-deck">
                    <?php 
                        require_once("src/Controle/ControlePoupanca.class.php");
                        $controlePoupanca = new ControlePoupanca();
                        $contasPoupanca = $controlePoupanca->mostrarTodasPoupancas($_SESSION['id']);
                        $contador = 0;
                        foreach($contasPoupanca as $item){
                            $valorPoupancaAtual = $item->getSaldo();
                            //Verificação se há taxas a serem adicionadas à poupanca.
                            $verificacao = 0;
                            $dataPoupanca = $item->getData();
                            $dataAtual = strtotime(date("Y-m-d"));
                            $diferencaDatas = $dataAtual - strtotime($dataPoupanca);
                            if($diferencaDatas>=0){
                                while($verificacao == 0){
                                    $diferencaDatas = $dataAtual - strtotime($dataPoupanca);
                                    if($diferencaDatas>=0){
                                        //Armazenamento do valor que deve ser registrado na entrada.
                                        $valorPoupancaEntrada = $valorPoupancaAtual * 0.005;
                                        $valorPoupancaAtual = $valorPoupancaAtual + $valorPoupancaAtual * 0.005;

                                        //Preenchimento da Data do Registro Entrada (Entrada de Taxa da Poupança) antes que ocorra uma mudança dela.
                                        $entrada->setData($dataPoupanca);
                                        
                                        $dataPoupancaTime = strtotime($dataPoupanca.' + 1 month');
                                        $dataPoupanca = date('Y-m-d', $dataPoupancaTime);
                                        
                                        //Atualizando a data da próxima taxa e acrescentando a taxa ao valor
                                        $controlePoupanca->AtualizacaoMensal($item->getId());
                                        
                                        //Preenchimento dos outros campos de Registro Entrada
                                        $entrada->setValor($valorPoupancaEntrada);
                                        $entrada->setTipo("Taxa Mensal");
                                        $entrada->setUsuarioId($_SESSION['id']);
                                        $entrada->setPoupancaId($item->getId());
                                        $controleEntrada->criarRegistro($entrada);
                                    }else{
                                        $verificacao = 1;
                                    }
                                }
                            }
                            //Informações das poupanças
                            $dataTaxa = new DateTime($item->getData());
                            $contador++;
                            if($contador%5==0){
                    echo "
                    </div>
                    <div class='card-deck'>";
                            }
                    ?>
                        <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
                            <div class="card-header"><h4><span class="corP">Banco da Conta Poupança: </span><?php echo $item->getBanco(); ?></h4></div>
                            <div class="card-body">
                                <h5 class="card-title"><span class="corS">Próxima adição de taxa: </span><?php echo $dataTaxa->format("d/m/Y"); ?></h5>
                                <h5 class="card-text"><span class="corS">Saldo: </span>R$ <?php echo $item->getSaldo(); ?></h5>
                            </div>
                        </div>
                    <?php 
                        } 
                    ?>
                    </div>
                    <hr>
                    <center><h2><span class="text-muted">Contas Corrente</span></h2></center>
                    <hr>
                    <center>
                        <button type='button' class='btn btn-primary' data-toggle='modal' data-target='#modalCorrente'>Criar uma Conta Corrente</button>
                    </center>
                    <br>
                    <div class="card-deck">
                    <?php
                        $contador=0;
                        require_once("src/Controle/ControleCorrente.class.php");
                        $controleCorrente = new ControleCorrente();
                        $contasCorrente = $controleCorrente->mostrarTodasCorrentes($_SESSION['id']);
                        foreach($contasCorrente as $item){
                            $contador++;
                            if($contador%5==0){
                    echo "
                    </div>
                    <div class='card-deck'>";
                            }
                    ?>
                        <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
                            <div class="card-header"><h4><span class="corP">Banco da Conta Corrente: </span><?php echo $item->getBanco(); ?></h4></div>
                            <div class="card-body">
                                <h5 class="card-text"><span class="corS">Saldo: </span>R$ <?php echo $item->getSaldo(); ?></h5>
                            </div>
                        </div>
                    <?php
                        }
                    ?>
                    </div>
                    <hr>
                    <center><h2><span class="text-muted">Cartões</span></h2></center>
                    <hr>
                    <center>
                        <button type='button' class='btn btn-primary' data-toggle='modal' data-target='#modalCartao'>Criar um cartao</button>
                    </center>
                    <br>
                    <div class="card-deck">
                    <?php
                        $contador=0;
                        require_once("src/Controle/ControleCartao.class.php");
                        $controleCartao = new ControleCartao();
                        $cartoes = $controleCartao->mostrarTodosCartoes($_SESSION['id']);
                        foreach($cartoes as $item){
                            $contador++;
                            if($contador%5==0){
                    echo "
                    </div>
                    <div class='card-deck'>";
                            }
                    ?>
                        <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
                            <div class="card-header"><h4><span class="corP">Numero do cartao: </span><?php echo $item->getNumero(); ?></h4></div>
                            <div class="card-body">
                                <h5 class="card-text"><span class="corS">Tipo: </span><?php 
                                if($item->getTipo() == 'credeb'){
                                    echo "Crédito e Débito";
                                }else if($item->getTipo() == 'credito'){
                                    echo "Crédito";
                                }else{
                                    echo "Débito";
                                }
                                ?></h5>
                                <?php 
                                if($item->getTipo()== 'debito' or $item->getTipo()== 'credeb'){
                                    $cartaoCorrente = $controleCorrente->mostrarCorrente($item->getCorrenteId());
                                    echo "<h5 class='card-text'><span class='corS'>Banco: </span>" . $cartaoCorrente->getBanco() . "</h5>";
                                }
                                ?>
                                <?php if($item->getTipo()== 'credito' or $item->getTipo()== 'credeb'){ ?>
                                <h5 class="card-text"><span class="corS">Limite à Vista </span>R$ <?php echo $item->getLimiteVista(); ?></h5>
                                <h5 class="card-text"><span class="corS">Limite a Prazo </span>R$ <?php echo $item->getLimitePrazo(); ?></h5>
                                <h5 class="card-text"><span class="corS">Data de Vencimento da Fatura: </span>R$ <?php echo $item->getDataVencimento(); ?></h5>
                                <?php } ?>
                            </div>
                        </div>
                    <?php
                        }
                    ?>
                    </div>
                </div>
            <?php require_once("footer.php");?>
        </main>
        <?php require_once("modalHome.php");?>
    </body>
    <script src='js/jquery.js'></script>
    <script src="bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>
    <script src="bootstrap-4.3.1-dist/js/bootstrap.bundle.js"></script>
    <script src="js/ajax.js"></script>
    <script src='js/cartaoAjax.js'></script>
    <script>
        $("#carteira").submit(function(){
            var dadosCarteira = $(this).serialize();
            $.post("criarCarteira.php", dadosCarteira, function(resultado){
                if(resultado == 0){
                    $("#erroGeral").modal("show");
                }else if(resultado == 1){
                    $("#modalCarteira").modal("hide");
                    $("#sucessoCarteira").modal("show");
                }
            });
            return false;
        });
        $("#sucessoCarteira").on("hide.bs.modal", function(){
            window.location.replace("index.php");
        });
    </script>
    <script>
        $("#salario").submit(function(){
            var dadosSalario = $(this).serialize();
            $.post("criarSalario.php", dadosSalario, function(resultado){
                if(resultado == 0){
                    $("#erroGeral").modal("show");
                }else if(resultado == 1){
                    $("#modalSalario").modal("hide");
                    $("#sucessoSalario").modal("show");
                }
            });
            return false;
        });
        $("#sucessoSalario").on("hide.bs.modal", function(){
            window.location.replace("index.php");
        });
    </script>
    <script>
        $("#editarSalario").submit(function(){
            var dadosEditarSalario = $(this).serialize();
            $.post("alterarSalario.php", dadosEditarSalario, function(resultado){
                if(resultado == 0){
                    $("#erroGeral").modal("show");
                }else if(resultado == 1){
                    $("#modalEditarSalario").modal("hide");
                    $("#sucessoEditarSalario").modal("show");
                }
            });
            return false;
        });
        $("#sucessoEditarSalario").on("hide.bs.modal", function(){
            window.location.replace("index.php");
        });
    </script>
    <script>
        $("#senha").submit(function(){
            var dadosSenha = $(this).serialize();
            $.post("alterarSenha.php", dadosSenha, function(resultado){
                if(resultado == 0){
                     $("#erroGeral").modal("show");
                }else if(resultado == 1){
                    $("#modalSenha").modal("hide");
                    $("#sucessoSenha").modal("show");
                }else if(resultado == 2){
                    $("#erroSenhaDiferente").modal("show");
                }
            });
            return false;
        });
        $("#sucessoSenha").on("hide.bs.modal", function(){
            window.location.replace("index.php");
        });
    </script>
    <script>
        $("#poupanca").submit(function(){
            var dadosPoupanca = $(this).serialize();
            $.post("criarPoupanca.php", dadosPoupanca, function(resultado){
                if(resultado == 0){
                    $("#erroGeral").modal("show");
                }else if(resultado == 1){
                    $("#modalPoupanca").modal("hide");
                    $("#sucessoPoupanca").modal("show");
                }
            });
            return false;
        });
        $("#sucessoPoupanca").on("hide.bs.modal", function(){
            window.location.replace("index.php");
        });
    </script>
    <script>
        $("#corrente").submit(function(){
            var dadosCorrente = $(this).serialize();
            $.post("criarCorrente.php", dadosCorrente, function(resultado){
                if(resultado == 0){
                    $("#erroGeral").modal("show");
                }else if(resultado == 1){
                    $("#modalCorrente").modal("hide");
                    $("#sucessoCorrente").modal("show");
                }
            });
            return false;
        });
        $("#sucessoCorrente").on("hide.bs.modal", function(){
            window.location.replace("index.php");
        });
    </script>
    <script>
        $("#cartao").submit(function(){
            var dadosCartao = $(this).serialize();
            $.post("criarCartao.php", dadosCartao, function(resultado){
                if(resultado == 0){
                    $("#erroGeral").modal("show");
                }else if(resultado == 1){
                    $("#modalCartao").modal("hide");
                    $("#sucessoCartao").modal("show");
                }
            });
            return false; 
        });
        $("#sucessoCartao").on("hide.bs.modal", function(){
            window.location.replace("index.php");
        });
    </script>
</html>