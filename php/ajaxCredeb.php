<div class="form-group">
	<label for="recipient-limiteVista" class="col-from-label">Limite à Vista</label>
	<input type="number" name='limiteVista' step='.01' class="form-control" id="recipient-limiteVista" placeholder="Ex.: 1000,99" required>
</div>
<div class="form-group">
	<label for="recipient-limitePrazo" class="col-from-label">Limite a Prazo</label>
	<input type="number" name='limitePrazo' step='.01' class="form-control" id="recipient-limitePrazo" placeholder="Ex.: 1000,99" required>
</div>
<div class="form-group">
	<label for="recipient-dataVencimento" class="col-from-label">Data de Vencimento da Fatura: </label>
	<input type='number'min='1' max='30' name='dataVencimento' class="form-control" id="recipient-dataVencimento" required>
</div>
<div class="form-group">
	<label for="recipient-correnteId">Conta Corrente: </label>
	<select class="form-control" name='correnteId' id="recipient-correnteId" required>
		<option></option>
		<?php 
			require_once("src/Controle/ControleCorrente.class.php");
			session_start();
			$controleCorrente = new ControleCorrente();
			$contasCorrente = $controleCorrente->mostrarTodasCorrentes($_SESSION['id']);	
			foreach($contasCorrente as $item){
				echo "<option value=" . $item->getId() . ">" . $item->getBanco() . "</option>";
			}
		?>
	</select>
</div>