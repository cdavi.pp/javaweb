<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Home</title>
        <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.min.css">
    </head>
    <style>
        .corS{
            color: #BEBEBE;
        }
        .corP{
            color: #E6E6FA;
        }
        .corU{
            color: white;
        }
    </style>
    <body>
        <header>
           	<%@ taglib uri="http://java.sun/jsp/jstl/core" prefix="c" %>
           	<c:import url="navBar.jsp"><c:param name="title" value="Menu"/></c:import>
            <section class="jumbotron text-center">
                <div class="container">
                    <h1 class="jumbotron-heading"><span class="text-muted">X</span>Finanças</h1>
                    <p class="lead text-muted">As tabelas mostram suas informações. Caso elas forem editadas, você acompanha as mudanças simultaneamente. Também pode ver contas com a data próxima para pagar, entre outras <a href="home.php">funcionalidades.</a></p>
                    <div id="funcionalidades"></div>
                </div>
            </section>
        </header>
        <main role="main">
            <div class="content container marketing">
                <main role="main">
                    <div class="jumbotron corU bg-dark">
                        <div class="col-sm-10 mx-auto">
                            <h3><span class="corP">Usuário: </span>Bruno de Paiva Cruz</h3>
                            <h5><span class="corP">Email: </span> bru@bc.com</h5>
                            <h5><span class="corP">Saldo da Carteira: </span>R$ 1000</h5>
                            <button type="button" class="btn btn-primary">Alterar Senha</button>
                            
                        </div>
                    </div>
                </main>
                //Outra aba no Menu: Pagamentos não feitos<br>
                //Contas correntes<br>
                <!--Card Cartão -->
                <div class="card-deck">
                    <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
                        <div class="card-header"><span class="corP">Nº Cartão: </span>3456783272349</div>
                        <div class="card-body">
                            <h5 class="card-title"><span class="corS">Tipo: </span>Debito</h5>
                            <p class="card-text"><span class="corS">Limite a Vista:</span> R$ 1000</p>
                            <p class="card-text"><span class="corS">Limite a Prazo:</span> R$ 1200</p>
                            <p class="card-text"><span class="corS">Data de Vencimento:</span> 13/06/2020</p>
                        </div>
                    </div>
                    <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
                        <div class="card-header"><span class="corP">Nº Cartão: </span>3456783272349</div>
                        <div class="card-body">
                            <h5 class="card-title"><span class="corS">Tipo: </span>Debito</h5>
                            <p class="card-text"><span class="corS">Limite a Vista:</span> R$ 1000</p>
                            <p class="card-text"><span class="corS">Limite a Prazo:</span> R$ 1200</p>
                            <p class="card-text"><span class="corS">Data de Vencimento:</span> 13/06/2020</p>
                        </div>
                    </div>
                    <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
                        <div class="card-header"><span class="corP">Nº Cartão: </span>3456783272349</div>
                        <div class="card-body">
                            <h5 class="card-title"><span class="corS">Tipo: </span>Debito</h5>
                            <p class="card-text"><span class="corS">Limite a Vista:</span> R$ 1000</p>
                            <p class="card-text"><span class="corS">Limite a Prazo:</span> R$ 1200</p>
                            <p class="card-text"><span class="corS">Data de Vencimento:</span> 13/06/2020</p>
                        </div>
                    </div>
                    <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
                        <div class="card-header"><span class="corP">Nº Cartão: </span>3456783272349</div>
                        <div class="card-body">
                            <h5 class="card-title"><span class="corS">Tipo: </span>Debito</h5>
                            <p class="card-text"><span class="corS">Limite a Vista:</span> R$ 1000</p>
                            <p class="card-text"><span class="corS">Limite a Prazo:</span> R$ 1200</p>
                            <p class="card-text"><span class="corS">Data de Vencimento:</span> 13/06/2020</p>
                        </div>
                    </div>
                    
                </div>
                <div class="card-deck">
                    <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
                        <div class="card-header"><span class="corP">Nº Cartão: </span>3456783272349</div>
                        <div class="card-body">
                            <h5 class="card-title"><span class="corS">Tipo: </span>Debito</h5>
                            <p class="card-text"><span class="corS">Limite a Vista:</span> R$ 1000</p>
                            <p class="card-text"><span class="corS">Limite a Prazo:</span> R$ 1200</p>
                            <p class="card-text"><span class="corS">Data de Vencimento:</span> 13/06/2020</p>
                        </div>
                    </div>
                    <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
                        <div class="card-header"><span class="corP">Nº Cartão: </span>3456783272349</div>
                        <div class="card-body">
                            <h5 class="card-title"><span class="corS">Tipo: </span>Debito</h5>
                            <p class="card-text"><span class="corS">Limite a Vista:</span> R$ 1000</p>
                            <p class="card-text"><span class="corS">Limite a Prazo:</span> R$ 1200</p>
                            <p class="card-text"><span class="corS">Data de Vencimento:</span> 13/06/2020</p>
                        </div>
                    </div>
                    <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
                        <div class="card-header"><span class="corP">Nº Cartão: </span>3456783272349</div>
                        <div class="card-body">
                            <h5 class="card-title"><span class="corS">Tipo: </span>Debito</h5>
                            <p class="card-text"><span class="corS">Limite a Vista:</span> R$ 1000</p>
                            <p class="card-text"><span class="corS">Limite a Prazo:</span> R$ 1200</p>
                            <p class="card-text"><span class="corS">Data de Vencimento:</span> 13/06/2020</p>
                        </div>
                    </div>
                    <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
                        <div class="card-header"><span class="corP">Nº Cartão: </span>3456783272349</div>
                        <div class="card-body">
                            <h5 class="card-title"><span class="corS">Tipo: </span>Debito</h5>
                            <p class="card-text"><span class="corS">Limite a Vista:</span> R$ 1000</p>
                            <p class="card-text"><span class="corS">Limite a Prazo:</span> R$ 1200</p>
                            <p class="card-text"><span class="corS">Data de Vencimento:</span> 13/06/2020</p>
                        </div>
                    </div>
                    
                </div>
                

                //Div adicionar poupança -> em baixo registro de todas as poupanças<br>
                //Div adicionar corrente -> em baixo registro de todas as correntes<br>

            </div>
           		<c:import url="footer.jsp"><c:param name="title" value="footer"/></c:import>
        </main>
    </body>
    <script src="bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>
    <script src="bootstrap-4.3.1-dist/js/bootstrapJquery.js"></script>
    <script src="bootstrap-4.3.1-dist/js/bootstrap.bundle.js"></script>
</html>