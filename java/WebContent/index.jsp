<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
if(request.getSession().getAttribute("id") != null){
	request.getRequestDispatcher("frontSis.jsp").forward(request, response);
}
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>XFinanças</title>
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/carrossel.css">
    <link rel="stylesheet" href="css/style.css">
    
</head>
<body>
    <header>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="#"><span class="text-muted">X</span>Finanças</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#funcionalidades">Funcionalidades <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="#sobre">Sobre <span class="sr-only">(current)</span></a>
                </li>
            </ul>
            <ul class="nav justify-content-end">
                <li class="nav-item">
                    <button type="button" class="btn btn-outline-success mr-sm-2" data-toggle="modal" data-target='#modalCadastro' id="botaoCadastro">
                        Cadastro
                    </button>
                </li>
                <li class="nav-item">
                    <button type="button" class="btn btn-outline-primary my-2 my-sm-0" data-toggle="modal" data-target="#modalLogin">
                        Login
                    </button>
                </li>
            </ul>
            </div>
        </nav>
    </header>
    <main role="main">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">    
	            <div class="carousel-item active">
	                <img src="img/imagem01.png" class="bd-placeholder-img" width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img"><rect width="100%" height="100%" fill="black"/>
	                <div class="container">
		                <div class="carousel-caption text-left">
		                    <h1>Organize você mesmo suas finanças</h1>
		                    <p>Esse site de possibilita cuidar você mesmo de suas finanças de maneira simples.</p>
		                    <p><a class="btn btn-lg btn-primary" href='#' data-toggle='modal' data-target='#modalCadastro' role="button">Registre-se</a></p>
		                </div>
	                </div>
	            </div>
	        </div>
        </div>
        
        
        <!-- Marketing messaging and featurettes
        ================================================== -->
        <!-- Wrap the rest of the page in another container to center all the content. -->
        
        <div class="container marketing" id="funcionalidades">
            <center><h1>Funcionalidades</h1></center><br>
            <!-- Three columns of text below the carousel -->
            <div class="row">
            <div class="col-lg-4">
                <img class="bd-placeholder-img rounded-circle" src="img/registro.png" width="140" height="140" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140"><title>Placeholder</title><rect width="100%" height="100%" fill="#777"/></img>
                <h2>Registro</h2>
                <p>Cada ação realizada como entrada e saída de dinheiro, como salário ou contas pagas, são registradas.</p>
                <p><button id="registro" class="btn btn-secondary" href="#" role="button">Detalhes &raquo;</button></p>
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-4">
                <img class="bd-placeholder-img rounded-circle" src="img/contas.png" width="140" height="140" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140"><title>Placeholder</title><rect width="100%" height="100%" fill="#777"/></img>
                <h2>Lembretes</h2>
                <p>Uma das funcionalidades do nosso sistema é lembrar quando está perto de expirar dividas a pagar.</p>
                <p><button id="lembrete" class="btn btn-secondary" href="#" role="button">Detalhes &raquo;</button></p>
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-4">
                <img class="bd-placeholder-img rounded-circle" src="img/data.png" width="140" height="140" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140"><title>Placeholder</title><rect width="100%" height="100%" fill="#777"/></img>
                <h2>Gerenciar contas</h2>
                <p>Você pode simular o controle de suas contas, dividas, entrada e saída de dinheiro de forma simples e rápida para uma melhor organização de suas finanças. </p>
                <p><button id="controle" class="btn btn-secondary" href="#" role="button">Detalhes &raquo;</button></p>
            </div><!-- /.col-lg-4 -->
            <div id="conteudo"></div>
        </div><!-- /.row -->
        
            </div><!-- /.container -->
            <!-- START THE FEATURETTES -->
        <div class="planof" id="sobre">
            <div class="container marketing">
            <hr class="featurette-divider">
        
            <div class="row featurette">
            <div class="col-md-7">
                <h2 class="featurette-heading">Ferramenta de fácil uso e <span class="text-muted"> facilita sua vida financeira.</span></h2>
                <p class="lead">Gerencie suas finanças facilmente com o nosso site.</p>
            </div>
            <div class="col-md-5">
                <div class="card">
                    <div class="card-body planoc">
                        <img src="img/imagem05.jpg" class="bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto" width="500" height="500" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 500x500"></img>
                    </div>
                </div>
            </div>
            </div>
        
            <hr class="featurette-divider">
            <div class="row featurette">
            <div class="col-md-7 order-md-2">
                <h2 class="featurette-heading">Faça você mesmo o controle <span class="text-muted">de suas contas</span></h2>
                <p class="lead">Com os registros você não ficará perdido com o que gastou ou o que ganhou...</p>
            </div>
            <div class="col-md-5 order-md-1">
                <div class="card">
                    <div class="card-body planoc">
                        <img src="img/imagem04.jpeg" class="bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto" width="500" height="500" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 500x500"></img>
                    </div>
                </div>
            </div>
            </div>
        
            <hr class="featurette-divider">
            <!-- /END THE FEATURETTES -->
        
        <!-- FOOTER -->

        <footer class="container">
            <p class="float-right"><a href="#">Voltar ao começo</a></p>
        	
        </footer>
    </main>
  <!-- Modal -->
	<c:import url="modalIndex.html"></c:import>
</body>
<script src='js/jquery.js'></script>
<script src="bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>
<script src="bootstrap-4.3.1-dist/js/bootstrap.bundle.js"></script>
<script src="js/ajax.js"></script>
<script src="js/app.js"></script>
<script>
	$("#cadastrarUsuario").submit(function(){
		var dadosCadastro = $(this).serialize();
		$.post("CadastrarUsuario", dadosCadastro, function(resultado){
			if(resultado == 0){
                $("#modalCadastro").modal("hide");
				$("#erroCadastro").modal("show");
			}else if(resultado == 1){
                $("#modalCadastro").modal("hide");
				$("#sucessoCadastro").modal("show");
			}else if(resultado == 2){
                $("#modalCadastro").modal("hide");
                $("#erroEmailCadastro").modal("show");
            }
		});
	   return false;
	});
</script>
<script>
	$("#login").submit(function(){
		var dadosLogin = $(this).serialize();
		$.post("Login", dadosLogin, function(resultado){
			if(resultado == 0){
                $("#modalLogin").modal("hide");
				$("#erroLogin").modal("show");
			}else if(resultado == 1){
				window.location.replace("index.jsp");
			}
		});
        return false;
	});
</script>
</html>