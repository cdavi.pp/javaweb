$('#recipient-forma').change(function() {
    if ($(this).val() === 'carteira') {
        requisitarArquivo("ajaxCarteira.php", ajaxEntrada);
    }else if ($(this).val() === 'poupanca'){
    	requisitarArquivo("ajaxEntradaPoupanca.php", ajaxEntrada);
    }else if ($(this).val() === 'corrente'){
    	requisitarArquivo("ajaxEntradaCorrente.php", ajaxEntrada);
    }
});

$('#recipient-tipo').change(function() {
    if ($(this).val() === 'outroTipo') {
    	requisitarArquivo("ajaxOutroTipo.html", ajaxOutroTipo);
    }
});