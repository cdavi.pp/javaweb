<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Registros de Entrada</title>
        <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.min.css">
    </head>
    <body>
        <header>
            <%@ taglib uri="http://java.sun/jsp/jstl/core" prefix="c" %>
           	<c:import url="navBar.jsp"><c:param name="title" value="Menu"/></c:import>
            <section class="jumbotron text-center">
                <div class="container">
                    <h1 class="jumbotron-heading">Registros de Entrada</h1>
                    <p class="lead text-muted">Todas as entradas são registradas e essas informações são mostradas nesta página</a></p>
                    <div id="funcionalidades"></div>
                </div>
            </section>
        </header>
        <main role="main">
            <div class="content container marketing">
                <center><h2>Entradas</h2></center>
                <table class="table table-striped table-dark">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">First</th>
                        <th scope="col">Last</th>
                        <th scope="col">Handle</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <th scope="row">1</th>
                        <td>Bruno Gasoso</td>
                        <td>bla</td>
                        <td>@mdo</td>
                        </tr>
                        <tr>
                        <th scope="row">2</th>
                        <td>Jacob</td>
                        <td>Thornton</td>
                        <td>@fat</td>
                        </tr>
                        <tr>
                        <th scope="row">3</th>
                        <td>Larry</td>
                        <td>the Bird</td>
                        <td>@twitter</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            	<c:import url="footer.jsp"><c:param name="title" value="footer"/></c:import>
        </main>
    </body>
    <script src="bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>
    <script src="bootstrap-4.3.1-dist/js/bootstrapJquery.js"></script>
    <script src="bootstrap-4.3.1-dist/js/bootstrap.bundle.js"></script>
</html>