<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>XFinanças</title>
        <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/carrossel.css">
        <link rel="stylesheet" href="css/style.css">
        
    </head>
    <body>
        <header>
            <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
                <a class="navbar-brand" href="#"><span class="text-muted">X</span>Finanças</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#funcionalidades">Funcionalidades <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="#sobre">Sobre <span class="sr-only">(current)</span></a>
                    </li>
                </ul>
                <ul class="nav justify-content-end">
                    <li class="nav-item">
                        <button type="button" class="btn btn-outline-success mr-sm-2" data-toggle="modal" data-target="#exampleModal">
                            Cadastro
                        </button>
                    </li>
                    <li class="nav-item">
                        <button type="button" class="btn btn-outline-primary my-2 my-sm-0" data-toggle="modal" data-target="#exampleModal02">
                            Login
                        </button>
                    </li>
                </ul>
                </div>
            </nav>
        </header>
        <main role="main">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    
                <div class="carousel-item active">
                    <img src="img/imagem01.png" class="bd-placeholder-img" width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img"><rect width="100%" height="100%" fill="black"/>
                    <div class="container">
                    <div class="carousel-caption text-left">
                        <h1>Organize você mesmo suas finanças</h1>
                        <p>Esse site de possibilita cuidar você mesmo de suas finanças de maneira simples.</p>
                        <p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p>
                    </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="img/imagem02.png" class="bd-placeholder-img" width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img"><rect width="100%" height="100%" fill="black"/>
                    <div class="container">
                    <div class="carousel-caption text-left">
                        <h1>Example headline.</h1>
                        <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                        <p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p>
                    </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="img/imagem03.jpg" class="bd-placeholder-img" width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img"><rect width="100%" height="100%" fill="black"/>
                    <div class="container">
                    <div class="carousel-caption text-left">
                        <h1>Example headline.</h1>
                        <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                        <p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p>
                    </div>
                    </div>
                </div>
                </div>
                <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
                </a>
            </div>
            
            
            <!-- Marketing messaging and featurettes
            ================================================== -->
            <!-- Wrap the rest of the page in another container to center all the content. -->
            
            <div class="container marketing" id="funcionalidades">
                <center><h1>Funcionalidades</h1></center><br>
                <!-- Three columns of text below the carousel -->
                <div class="row">
                <div class="col-lg-4">
                    <img class="bd-placeholder-img rounded-circle" src="img/registro.png" width="140" height="140" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140"><title>Placeholder</title><rect width="100%" height="100%" fill="#777"/></img>
                    <h2>Registro</h2>
                    <p>Cada ação realizada como entrada e saída de dinheiro, como salário ou contas pagas, são registradas.</p>
                    <p><button id="registro" class="btn btn-secondary" href="#" role="button">Detalhes &raquo;</button></p>
                </div><!-- /.col-lg-4 -->
                <div class="col-lg-4">
                    <img class="bd-placeholder-img rounded-circle" src="img/contas.png" width="140" height="140" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140"><title>Placeholder</title><rect width="100%" height="100%" fill="#777"/></img>
                    <h2>Lembretes</h2>
                    <p>Uma das funcionalidades do nosso sistema é lembrar quando está perto de expirar dividas a pagar.</p>
                    <p><button id="lembrete" class="btn btn-secondary" href="#" role="button">Detalhes &raquo;</button></p>
                </div><!-- /.col-lg-4 -->
                <div class="col-lg-4">
                    <img class="bd-placeholder-img rounded-circle" src="img/data.png" width="140" height="140" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140"><title>Placeholder</title><rect width="100%" height="100%" fill="#777"/></img>
                    <h2>Gerenciar contas</h2>
                    <p>Você pode simular o controle de suas contas, dividas, entrada e saída de dinheiro de forma simples e rápida para uma melhor organização de suas finanças. </p>
                    <p><button id="controle" class="btn btn-secondary" href="#" role="button">Detalhes &raquo;</button></p>
                </div><!-- /.col-lg-4 -->
                <div id="conteudo"></div>
            </div><!-- /.row -->
            
                </div><!-- /.container -->
                <!-- START THE FEATURETTES -->
            <div class="planof" id="sobre">
                <div class="container marketing">
                <hr class="featurette-divider">
            
                <div class="row featurette">
                <div class="col-md-7">
                    <h2 class="featurette-heading">Ferramenta de fácil uso e <span class="text-muted"> facilita sua vida financeira.</span></h2>
                    <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
                </div>
                <div class="col-md-5">
                    <div class="card">
                        <div class="card-body planoc">
                            <img src="img/imagem05.jpg" class="bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto" width="500" height="500" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 500x500"></img>
                        </div>
                    </div>
                </div>
                </div>
            
                <hr class="featurette-divider">
                <div class="row featurette">
                <div class="col-md-7 order-md-2">
                    <h2 class="featurette-heading">Faça você mesmo o controle <span class="text-muted">de suas contas</span></h2>
                    <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
                </div>
                <div class="col-md-5 order-md-1">
                    <div class="card">
                        <div class="card-body planoc">
                            <img src="img/imagem04.jpeg" class="bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto" width="500" height="500" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 500x500"></img>
                        </div>
                    </div>
                </div>
                </div>
            
                <hr class="featurette-divider">
            
                <div class="row featurette">
                <div class="col-md-7">
                    <h2 class="featurette-heading">Use em <span class="text-muted">qualquer lugar.</span></h2>
                    <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
                </div>
                <div class="col-md-5">
                    <div class="card">
                        <div class="card-body planoc">
                            <img src="img/imagem06.jpg" class="bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto" width="500" height="500" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 500x500"></img>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            
                <hr class="featurette-divider">
            </div>
                <!-- /END THE FEATURETTES -->
            
            <!-- FOOTER -->

            <footer class="container">
                <p class="float-right"><a href="#">Voltar ao começo</a></p>
                <p>&copy; 2017-2019 Company, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
            </footer>
        </main>
      <!-- Modal -->
      <div class="modal fade" id="exampleModal02" tabindex="-1" role="dialog" aria-labelledby="exampleModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title w-100 font-weight-bold" id="exampleModalTitle">Login</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body mx-3">
              <form action="">
                <div class="form-group">
                  <label for="recipient-email" class="col-from-label">Email</label>
                  <input type="email" class="form-control" id="recipient-email" placeholder="Digite seu Email...">
                </div>
                <div class="form-group">
                  <label for="recipient-password" class="col-from-label">Senha</label>
                  <input type="password" class="form-control" id="recipient-password" placeholder="Digite sua Senha...">
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button class="btn btn-secondary" data-dismiss="modal" type="button">Fechar</button>
              <button class="btn btn-primary" type="button">Enviar</button>
            </div>
          </div>
        </div>
      </div>
      
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title w-100 font-weight-bold" id="exampleModalTitle">Cadastro</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body mx-3">
              <form action="">
                <div class="form-group">
                  <label for="recipient-name" class="col-from-label">Nome</label>
                  <input type="text" class="form-control" id="recipient-name" placeholder="Digite seu Nome...">
                </div>
                <div class="form-group">
                  <label for="recipient-email" class="col-from-label">Email</label>
                  <input type="email" class="form-control" id="recipient-email" placeholder="Digite seu Email...">
                </div>
                <div class="form-group">
                  <label for="recipient-password" class="col-from-label">Senha</label>
                  <input type="password" class="form-control" id="recipient-password" placeholder="Digite sua Senha...">
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button class="btn btn-secondary" data-dismiss="modal" type="button">Fechar</button>
              <button class="btn btn-primary" type="button">Enviar</button>
            </div>
          </div>
        </div>
      </div>
    </body>
    <script src="bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>
    <script src="bootstrap-4.3.1-dist/js/bootstrapJquery.js"></script>
    <script src="bootstrap-4.3.1-dist/js/bootstrap.bundle.js"></script>
    <script src="js/ajax.js"></script>
    <script src="js/app.js"></script>
</html>