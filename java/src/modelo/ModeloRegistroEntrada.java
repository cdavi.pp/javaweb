package modelo;

public class ModeloRegistroEntrada {
	private int id;
	private float valor;
	private String data;
	private String tipo;
	private int usuarioId;
	private int poupancaId;
	private int correnteId;
	private int carteiraId;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public float getValor() {
		return valor;
	}
	public void setValor(float valor) {
		this.valor = valor;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public int getUsuarioId() {
		return usuarioId;
	}
	public void setUsuarioId(int usuarioId) {
		this.usuarioId = usuarioId;
	}
	public int getPoupancaId() {
		return poupancaId;
	}
	public void setPoupancaId(int poupancaId) {
		this.poupancaId = poupancaId;
	}
	public int getCorrenteId() {
		return correnteId;
	}
	public void setCorrenteId(int correnteId) {
		this.correnteId = correnteId;
	}
	public int getCarteiraId() {
		return carteiraId;
	}
	public void setCarteiraId(int carteiraId) {
		this.carteiraId = carteiraId;
	}
	
}
