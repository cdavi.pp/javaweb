package modelo;

public class ModeloCartao {
	private int id;
	private int numero;
	private float limiteAVista;
	private float limiteAPrazo;
	private int melhorDia;
	private String tipo;
	private int dataVencimento;
	private int usuarioId;
	private int correnteId;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public float getLimiteAVista() {
		return limiteAVista;
	}
	public void setLimiteAVista(float limiteAVista) {
		this.limiteAVista = limiteAVista;
	}
	public float getLimiteAPrazo() {
		return limiteAPrazo;
	}
	public void setLimiteAPrazo(float limiteAPrazo) {
		this.limiteAPrazo = limiteAPrazo;
	}
	public int getMelhorDia() {
		return melhorDia;
	}
	public void setMelhorDia(int melhorDia) {
		this.melhorDia = melhorDia;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public int getDataVencimento() {
		return dataVencimento;
	}
	public void setDataVencimento(int dataVencimento) {
		this.dataVencimento = dataVencimento;
	}
	public int getUsuarioId() {
		return usuarioId;
	}
	public void setUsuarioId(int usuarioId) {
		this.usuarioId = usuarioId;
	}
	public int getCorrenteId() {
		return correnteId;
	}
	public void setCorrenteId(int correnteId) {
		this.correnteId = correnteId;
	}
	
}
