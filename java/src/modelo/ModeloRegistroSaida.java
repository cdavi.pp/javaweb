package modelo;

public class ModeloRegistroSaida {
	private int id;
	private float valor;
	private String dataRegistro;
	private String dataPagamento;
	private String tipo;
	private int status;
	private int usuarioId	;
	private int cartaoId;
	private int correnteId;
	private int carteiraId;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public float getValor() {
		return valor;
	}
	public void setValor(float valor) {
		this.valor = valor;
	}
	public String getDataRegistro() {
		return dataRegistro;
	}
	public void setDataRegistro(String dataRegistro) {
		this.dataRegistro = dataRegistro;
	}
	public String getDataPagamento() {
		return dataPagamento;
	}
	public void setDataPagamento(String dataPagamento) {
		this.dataPagamento = dataPagamento;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getUsuarioId() {
		return usuarioId;
	}
	public void setUsuarioId(int usuarioId) {
		this.usuarioId = usuarioId;
	}
	public int getCartaoId() {
		return cartaoId;
	}
	public void setCartaoId(int cartaoId) {
		this.cartaoId = cartaoId;
	}
	public int getCorrenteId() {
		return correnteId;
	}
	public void setCorrenteId(int correnteId) {
		this.correnteId = correnteId;
	}
	public int getCarteiraId() {
		return carteiraId;
	}
	public void setCarteiraId(int carteiraId) {
		this.carteiraId = carteiraId;
	}
	
}
