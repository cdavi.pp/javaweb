package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import controle.ControleCarteira;

@WebServlet("/adicionarFundosCarteira")
public class AdicionarFundosCarteira extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ControleCarteira cc= new ControleCarteira();
		ModeloCarteira id=null;
		float valor=Float.valueOf(request.getParameter("valor"));
		if(cc.adicionarFundos(id, valor)) {
			response.getWriter().print("<script>alert('deu certo');window.location.href='frontSis.jsp';<script>");
		}else {
			response.getWriter().print("<script>alert('deu errado');window.location.href='frontSis.jsp';<script>");
		}
	}

}
