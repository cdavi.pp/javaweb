package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import controle.ControlePoupanca;

/**
 * Servlet implementation class adicionarFundos
 */
@WebServlet("/AdicionarFundos")
public class AdicionarFundosPoupanca extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ControlePoupanca controleP = new ControlePoupanca();
		int poupancaId = Integer.valueOf(request.getParameter("poupancaId"));
		float valor = Float.valueOf(request.getParameter("valor"));
		if(controleP.adicionarFundos(poupancaId, valor)) {
			response.getWriter().print("<script>alert('Deu certo');window.location.href='frontSis.jsp';</script>");
		}else {
			response.getWriter().print("<script>alert('N�o deu certo');window.location.href='frontSis.jsp';</script>");
		}
	}

}
