package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.ModeloCarteira;
import controle.ControleCarteira;

@WebServlet("/CriarCarteira")
public class CriarCarteira extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession sessao = request.getSession();
		ModeloCarteira mc= new ModeloCarteira();
		ControleCarteira cc= new ControleCarteira();
		int id = (Integer) sessao.getAttribute("id");
		float saldo=Float.valueOf(request.getParameter("saldo"));
		mc.setSaldo(saldo);
		mc.setId(id);
		if(cc.criarCarteira(mc)) {
			response.getWriter().print(1);
		}else {
			response.getWriter().print(0);
		}
	}

}
