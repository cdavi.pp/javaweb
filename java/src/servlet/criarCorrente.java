package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.ModeloCorrente;
import controle.ControleCorrente;

@WebServlet("/criarCorrente")
public class criarCorrente extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ModeloCorrente mc= new ModeloCorrente();
		ControleCorrente cc= new ControleCorrente();
		int id= Integer.valueOf(request.getParameter("id"));
		float saldo=Float.valueOf(request.getParameter("saldo"));
		String banco=request.getParameter("banco");
		boolean sessao= true;
		HttpSession session = request.getSession(sessao);
		session.setAttribute("id", id);
		if(cc.criarCorrente(mc)) {
			
		}
	}

}
