package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modelo.ModeloSalario;
import controle.controleSalario;

/**
 * Servlet implementation class criarSalario
 */
@WebServlet("/CriarSalario")
public class CriarSalario extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ModeloSalario salario = new ModeloSalario();
		controleSalario controleS = new controleSalario();
		int id = Integer.valueOf(request.getParameter("id"));
		float valor = Float.valueOf(request.getParameter("valor"));
		String data = request.getParameter("data");
		boolean sessao = true;
		HttpSession session = request.getSession(sessao);
		session.setAttribute("id", id);
		try {
			salario.setValor(valor);
			salario.setData(data);
			salario.setUsuarioId(id);
			controleS.criarSalario(salario);
			if(salario !=null) {
				response.getWriter().print("<script>alert('Deu certo');window.location.href='frontSis.jsp';</script>");
			}
		}catch(Exception e) {
			e.getMessage();
		}

		
	}

}
