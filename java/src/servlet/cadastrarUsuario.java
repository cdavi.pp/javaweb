package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.ModeloUsuario;
import controle.ControleUsuario;

@WebServlet("/CadastrarUsuario")
public class CadastrarUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ModeloUsuario mu= new ModeloUsuario();
		ControleUsuario cu= new ControleUsuario();
		String nome=request.getParameter("nome");
		String email=request.getParameter("email");
		String senha=request.getParameter("senha");
		if(cu.verificarEmail(email)) {
			try {
				mu.setNome(nome);
				mu.setEmail(email);
				mu.setSenha(senha);
				if(cu.adicionarUsuario(mu)) {
					response.getWriter().print(1);
				}else {
					response.getWriter().print(0);
				}
			}catch(Exception e){
				e.getMessage();
			}
		}else {
			response.getWriter().print(2);
		}
	}
}