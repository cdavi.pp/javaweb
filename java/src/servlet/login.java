package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.ModeloUsuario;
import controle.ControleUsuario;
/**
 * Servlet implementation class login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ModeloUsuario mu= new ModeloUsuario(); 
		ControleUsuario cu= new ControleUsuario();
		String email = request.getParameter("email");
		String senha = request.getParameter("senha");
		try {
			mu.setEmail(email);
			mu.setSenha(senha);
			int id = cu.logarUsuario(mu);
			if(id != 0) {
				HttpSession session = request.getSession();
				session.setAttribute("id", id);
				response.getWriter().print(1);
			}else {
				response.getWriter().print(0);
			}
		}catch(Exception e){
			e.getMessage();
		}
	}

}
