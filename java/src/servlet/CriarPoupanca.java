package servlet;


import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import controle.controlePoupanca;
import modelo.ModeloPoupanca;

/**
 * Servlet implementation class CriarPoupanca
 */
@WebServlet("/CriarPoupanca")
public class CriarPoupanca extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ModeloPoupanca poupanca = new ModeloPoupanca();
		controlePoupanca controleP = new controlePoupanca();
		float saldo = Float.valueOf(request.getParameter("saldo"));
		String data = request.getParameter("data");
		String banco = request.getParameter("banco");
		int usuarioId = Integer.valueOf(request.getParameter("usuarioId")); 
		try {
			poupanca.setSaldo(saldo);
			poupanca.setData(data);
			poupanca.setBanco(banco);
			poupanca.setUsuarioId(usuarioId);
			controleP.criarPoupanca(poupanca);
			if(poupanca !=null) {
				response.getWriter().print("<script>alert('Deu certo');window.location.href='frontSis.jsp';</script>");
			}
		}catch(Exception e) {
			e.getMessage();
		}
	}

}
