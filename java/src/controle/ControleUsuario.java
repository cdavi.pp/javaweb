 package controle; 
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import modelo.ModeloUsuario;
public class ControleUsuario{
    public boolean adicionarUsuario(ModeloUsuario user){
        boolean resultado = false;
        try{
            Connection con = new Conexao().abrirConexao();
            String sql = "INSERT INTO usuario(nome, email, senha) VALUES(?,?,?);";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, user.getNome());
            ps.setString(2, user.getEmail());
            ps.setString(3, user.getSenha());
            if(!ps.execute()) {
                resultado = true;
            }
            new Conexao().fecharConexao(con);
		}catch(SQLException e){
			System.out.println(e.getMessage());
		}
		return resultado;
    }

    public int logarUsuario(ModeloUsuario user){
    	int idUser = 0;
        try {
			Connection con = new Conexao().abrirConexao();
			String sql = "SELECT id, count(id) as c FROM usuario WHERE email=? AND senha=?;";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, user.getEmail());
			ps.setString(2, user.getSenha());
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				while(rs.next()) {
					idUser = rs.getInt("id");
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return idUser;
    }

    public boolean alterarSenha(ModeloUsuario user){
        boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "UPDATE usuario SET senha=? WHERE id=?;";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, user.getId());
			ps.setString(2, user.getSenha());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
    }

	public boolean verificarEmail(String email){
		boolean resultado = false;
		try{
			Connection con=new Conexao().abrirConexao();
			String sql="SELECT count(id) AS c From usuario WHERE email=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, email);
			ResultSet rs=ps.executeQuery();
			while(rs.next()) {
				if(rs.getInt("c")==0) {
					resultado = true;
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e){
			System.out.println(e.getMessage());
		}
		return resultado;
	}

	public ModeloUsuario mostrarInformacoes(int id){
		ModeloUsuario usuario= new ModeloUsuario();
		try{
			Connection con= new Conexao().abrirConexao();
			String sql="SELECT nome, email FROM usuario WHERE id = ?;";
			PreparedStatement ps=con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs=ps.executeQuery();
			if(rs!=null){
				usuario.setNome(rs.getString("nome"));
				usuario.setEmail(rs.getString("email"));
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e){
			System.out.println(e.getMessage());
		}
		return usuario;
	}

}