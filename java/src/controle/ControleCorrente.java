package controle;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import modelo.ModeloCorrente;
public class ControleCorrente{
  public boolean criarCorrente(ModeloCorrente user) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "INSERT INTO corrente(saldo, banco, usuario_id) VALUES(?,?,?);";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setFloat(1, user.getSaldo());
            ps.setString(2, user.getBanco());
            ps.setInt(3, user.getUsuarioId());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}

  public ModeloCorrente pegarInformacoes(int id) {
		ModeloCorrente corrente = new ModeloCorrente();
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "SELECT * FROM corrente WHERE usuario_id=?;";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				corrente.setId(rs.getInt("id"));
				corrente.setSaldo(rs.getInt("saldo"));
                corrente.setBanco(rs.getString("banco"));
                corrente.setUsuarioId(rs.getInt("usuario_id"));
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return corrente;
	}

  public boolean adicionarFundos(int id, float valor) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "UPDATE corrente SET saldo = saldo + ? WHERE id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setFloat(1, valor);
			ps.setInt(2, id);
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}

  public boolean retirarFundos(int id, float valor) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "UPDATE corrente SET saldo = saldo - ? WHERE id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setFloat(1, valor);
			ps.setInt(2, id);
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
  
  public ArrayList<ModeloCorrente> mostrarTodasCorrentes() {
	  ArrayList<ModeloCorrente> lista = new ArrayList<ModeloCorrente>();
	  try {
		  Connection con = new Conexao().abrirConexao();
		  String sql="SELECT * FROM corrente;";
		  PreparedStatement ps = con.prepareStatement(sql);
		  ResultSet rs=ps.executeQuery();
		  if(rs != null){
    		  while(rs.next()) {
    		      ModeloCorrente corrente= new ModeloCorrente();
    			  corrente.setSaldo(rs.getFloat("saldo"));
    			  corrente.setBanco(rs.getString("banco"));
    			  corrente.setUsuarioId(rs.getInt("usuario_id"));
    			  lista.add(corrente);
    		  }
		  }
		  new Conexao().fecharConexao(con);
	  }catch(SQLException e) {
		  System.out.println(e.getMessage());
	  }
	  return lista;
  }
}