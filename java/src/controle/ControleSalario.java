package controle;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Connection;
import modelo.ModeloSalario;

public class ControleSalario {
	public boolean criarSalario(ModeloSalario salario) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "INSERT INTO salario(valor, data, usuario_id) VALUES(?,?,?);";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setFloat(1, salario.getValor());
			ps.setString(2, salario.getData());
    	ps.setInt(3, salario.getUsuarioId());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
    public ModeloSalario informacao(int id) {
    	ModeloSalario salario = null;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "SELECT * FROM salario WHERE usuario_id = ?;";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				salario = new ModeloSalario();
				salario.setId(rs.getInt("id"));
				salario.setValor(rs.getInt("valor"));
				salario.setData(rs.getString("data"));
				salario.setUsuarioId(rs.getInt("usuario_id"));
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return salario;
	}
    public boolean alterarSalario(ModeloSalario salario) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "UPDATE salario SET valor=? WHERE usuario_id=?;";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setFloat(1, salario.getValor());
			ps.setInt(2, salario.getId());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
    public boolean entradaMensal(ModeloSalario salario) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "UPDATE salario SET data= ADDDATE(data, INTERVAL 1 MONTH) WHERE usuario_id=:id;";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, salario.getData());
			ps.setInt(2, salario.getId());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
}
