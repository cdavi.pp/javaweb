package controle;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.Connection;
import modelo.ModeloPoupanca;

public class ControlePoupanca {
	public boolean criarPoupanca(ModeloPoupanca poupanca) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "INSERT INTO poupanca(valor, data, banco, usuario_id) VALUES(?,?,?,?);";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setFloat(1, poupanca.getSaldo());
			ps.setString(2, poupanca.getData());
		    ps.setString(2, poupanca.getBanco());
		    ps.setInt(2, poupanca.getUsuarioId());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
    public ModeloPoupanca mostrarPoupanca(int id) {
		ModeloPoupanca poupanca = null;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "SELECT * FROM poupanca WHERE usuario_id=?;";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				poupanca = new ModeloPoupanca();
				poupanca.setId(rs.getInt("id"));
				poupanca.setSaldo(rs.getInt("saldo"));
				poupanca.setData(rs.getString("data"));
				poupanca.setBanco(rs.getString("banco"));
				poupanca.setUsuarioId(rs.getInt("usuario_id"));
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return poupanca;
	}
    public ArrayList<ModeloPoupanca> consultarTodasPoupancas(){
    	ArrayList<ModeloPoupanca> lista = new ArrayList<ModeloPoupanca>();
    	try {
    		Connection con = new Conexao().abrirConexao();
    		PreparedStatement ps = con.prepareStatement("SELECT * FROM poupanca;");
    		ResultSet rs = ps.executeQuery();
    		if(rs != null) {
    			while(rs.next()) {
    				ModeloPoupanca poupanca = new ModeloPoupanca();
    				poupanca.setId(rs.getInt("id"));
    				poupanca.setSaldo(rs.getInt("saldo"));
    				poupanca.setData(rs.getString("data"));
    				poupanca.setBanco(rs.getString("banco"));
    				poupanca.setUsuarioId(rs.getInt("usuario_id"));
    				lista.add(poupanca);
    			}
    		}
		}catch(SQLException e){
			System.out.println(e.getMessage());
		}
    	return lista;
    }
    public boolean adicionarFundos(int id, float valor) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "UPDATE poupanca SET saldo = saldo + ? WHERE id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setFloat(1, valor);
			ps.setInt(2, id);
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
    public boolean retirarFundos(int id, float valor) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "UPDATE poupanca SET saldo = saldo - ? WHERE id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setFloat(1, valor);
			ps.setInt(2, id);
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
    public boolean atualizacaoMensal(ModeloPoupanca poupanca) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "UPDATE  poupanca SET saldo = (saldo * 0.005) + saldo, data = ADDDATE(data, INTERVAL 1 MONTH) WHERE id=:id;";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(2, poupanca.getId());
            ps.setString(1, poupanca.getData());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
}
