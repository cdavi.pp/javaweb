package controle;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import modelo.ModeloCarteira;
public class ControleCarteira{
  public boolean criarCarteira(ModeloCarteira user) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "INSERT INTO carteira (saldo, usuario_id) VALUES(?,?);";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setFloat(1, user.getSaldo());
			ps.setInt(2, user.getUsuarioId());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}

  public ModeloCarteira mostrarInformacoes(int id) {
	  	ModeloCarteira carteira= new ModeloCarteira();
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "SELECT id, saldo FROM carteira WHERE usuario_id=?;";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if(rs!=null) {
				carteira.setId(rs.getInt("id"));
				carteira.setSaldo(rs.getFloat("saldo"));
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return carteira;
	}

  public boolean adicionarFundos(ModeloCarteira car, float valor) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "UPDATE carteira SET saldo = saldo + ? WHERE id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setFloat(1, valor);
			ps.setInt(2, car.getId());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}

  public boolean retirarFundos(ModeloCarteira car, float valor) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "UPDATE carteira SET saldo = saldo - ? WHERE id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setFloat(1,valor);
			ps.setInt(2, car.getId());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}

}