DROP SCHEMA IF EXISTS sistdin;

CREATE SCHEMA sistdin;

USE sistdin;

CREATE TABLE usuario(
id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
nome VARCHAR(60) NOT NULL,
email VARCHAR(60) UNIQUE NOT NULL,
senha VARCHAR(60) NOT NULL
);

CREATE TABLE carteira(
id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
saldo FLOAT ,
usuario_id INT NOT NULL,
FOREIGN KEY(usuario_id) REFERENCES usuario(id)
);

CREATE TABLE poupanca(
id INT AUTO_INCREMENT PRIMARY KEY,
saldo FLOAT,
banco VARCHAR(60) NOT NULL,
data DATE NOT NULL,
usuario_id INT NOT NULL,
FOREIGN KEY(usuario_id) REFERENCES usuario(id)
);

CREATE TABLE corrente(
id INT AUTO_INCREMENT PRIMARY KEY,
saldo FLOAT,
banco VARCHAR(60) NOT NULL,
usuario_id INT NOT NULL,
FOREIGN KEY(usuario_id) REFERENCES usuario(id)
);

CREATE TABLE cartao(
id INT AUTO_INCREMENT PRIMARY KEY,
numero BIGINT(16) NOT NULL,
limiteVista FLOAT,
limitePrazo FLOAT,
melhorDia INT,
tipo VARCHAR(7) NOT NULL,
dataVencimento INT(2),
usuario_id INT NOT NULL,
corrente_id INT,
FOREIGN KEY(usuario_id) REFERENCES usuario(id),
FOREIGN KEY (corrente_id) REFERENCES corrente(id)
);

CREATE TABLE salario(
id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
valor FLOAT,
data DATE NOT NULL,
usuario_id INT NOT NULL,
corrente_id INT,
poupanca_id INT,
carteira_id INT,
FOREIGN KEY(usuario_id) REFERENCES usuario(id),
FOREIGN KEY(corrente_id) REFERENCES corrente(id),
FOREIGN KEY(poupanca_id) REFERENCES poupanca(id),
FOREIGN KEY(carteira_id) REFERENCES carteira(id)
);

CREATE TABLE registroEntrada(
id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
valor FLOAT NOT NULL,
data DATE NOT NULL,
tipo VARCHAR(60) NOT NULL,
usuario_id INT NOT NULL,
corrente_id INT,
poupanca_id INT,
carteira_id INT,
FOREIGN KEY(usuario_id) REFERENCES usuario(id),
FOREIGN KEY(corrente_id) REFERENCES corrente(id),
FOREIGN KEY(poupanca_id) REFERENCES poupanca(id),
FOREIGN KEY(carteira_id) REFERENCES carteira(id)
);

CREATE TABLE registroSaida(
id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
valor FLOAT NOT NULL,
dataRegistro DATE NOT NULL,
dataPagamento DATE NOT NULL,
tipo VARCHAR(60) NOT NULL,
status TINYINT NOT NULL DEFAULT 1,
usuario_id INT NOT NULL,
corrente_id INT,
cartao_id INT,
carteira_id INT,
FOREIGN KEY(usuario_id) REFERENCES usuario(id),
FOREIGN KEY(corrente_id) REFERENCES corrente(id),
FOREIGN KEY(cartao_id) REFERENCES cartao(id),
FOREIGN KEY(carteira_id) REFERENCES carteira(id)
);